package com.burov.database.fragment;

import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.burov.database.R;
import com.burov.database.loader.GoodsLoader;
import com.burov.database.loader.InsertExistGoodsLoader;
import com.burov.database.loader.ManufacturerLoader;
import com.burov.database.loader.SupplierLoader;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.provider.DataBaseContentProvider;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Vitaliy on 25-Dec-15.
 */
public class GoodsExistInsertFragment extends Fragment implements View.OnClickListener,
        android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor>, SupplierInsertDialog.SupplierInsertListener,
        ManufacturerInsertDialog.ManufacturerInsertListener {
    EditText eNameOfGoods;
    EditText eDescriptionOfGoods;
    Spinner sWeightOrPieces;
    EditText eWeightOfGoods;
    EditText eNumOfGoods;
    EditText eBasePrice;
    Spinner sSuppliers;
    Spinner sManufacturers;
    EditText eManufacturingDate;
    EditText eApplicationTime;
    EditText eDeliveryDateToGrocery = null;
    private double weightOfGoods;
    private int numOfGoods;
    private int piecesOrGrams;
    private String nameOfSupplier;
    private String nameOfManufacturer;
    private final String LOG_TAG = "myLogs";
    ContentValues cv = null;
    ContentResolver cr = null;
    FragmentTransaction fTrans;
    FloatingActionButton fab;
    private SimpleDateFormat dateFormatter;
    TextInputLayout layout_name_of_goods, layout_description_of_goods,
            layout_base_price_of_the_goods, layout_manufacturing_date,
            layout_application_time, layout_delivery_date_to_the_grocery,
            layout_weight_of_the_goods, layout_number_of_the_goods;
    private static final String ID_OF_THE_GROCERY = "ID_OF_THE_GROCERY";
    private static final String ID_OF_THE_GOODS = "ID_OF_THE_GOODS";
    long grocery_id = 0l;
    long goods_id = -1;
    private static final int LOADER_G_ID = 4;
    private static final int LOADER_S_ID = 5;
    private static final int LOADER_M_ID = 6;
    ArrayAdapter<String> adapterSuppliers;
    ArrayAdapter<String> adapterManufacturers;
    SupplierInsertDialog supplierInsertDialog;
    ManufacturerInsertDialog manufacturerInsertDialog;
    long supplier_id = 0;
    long manufacturer_id = 0;
    ArrayAdapter<CharSequence> adapterWeightOrPieces;
    private static final String NAME_OF_SUPPLIER = "nameOfSupplier";
    private static final String NAME_OF_MANUFACTURER = "nameOfManufacturer";

    public static GoodsExistInsertFragment newGoodsExistInsertFragment(long idOfGrocery, long idOfGoods) {
        GoodsExistInsertFragment fragment = new GoodsExistInsertFragment();
        Bundle args = new Bundle();
        args.putLong(ID_OF_THE_GROCERY, idOfGrocery);
        args.putLong(ID_OF_THE_GOODS, idOfGoods);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cr = getActivity().getContentResolver();
        grocery_id = getArguments().getLong(ID_OF_THE_GROCERY);
        goods_id = getArguments().getLong(ID_OF_THE_GOODS);
        cv = new ContentValues();
        supplierInsertDialog = new SupplierInsertDialog().newInstanceSupplierInsert();
        manufacturerInsertDialog = new ManufacturerInsertDialog().newInstanceManufacturerInsert();
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState!=null) {
            nameOfSupplier = savedInstanceState.getString(NAME_OF_SUPPLIER);
            if (nameOfSupplier.equals(getResources().getString(R.string.new_supplier))) {
                nameOfSupplier = null;
            }
            sSuppliers.setSelection(adapterSuppliers.getPosition(nameOfSupplier));
            nameOfManufacturer = savedInstanceState.getString(NAME_OF_MANUFACTURER);
            if (nameOfManufacturer.equals(getResources().getString(R.string.new_manufacturer))) {
                nameOfManufacturer = null;
            }
            sManufacturers.setSelection(adapterManufacturers.getPosition(nameOfManufacturer));
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        nameOfSupplier = sSuppliers.getSelectedItem().toString();
        nameOfManufacturer = sManufacturers.getSelectedItem().toString();
        outState.putString(NAME_OF_SUPPLIER, nameOfSupplier);
        outState.putString(NAME_OF_MANUFACTURER, nameOfManufacturer);
    }
    private void pickDateToEditText (final EditText e) {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        final Calendar mCalendar = Calendar.getInstance();
        DatePickerDialog mDatePicker = new DatePickerDialog
                (getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        mCalendar.set(selectedyear, selectedmonth, selectedday);
                        e.setText(dateFormatter.format(mCalendar.getTime()));
                    }
                }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
        mDatePicker.show();
    }
    private void onTextChangeListener(final EditText e, final TextInputLayout textInputLayout){
        e.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0)
                    e.setError(null);
                textInputLayout.setErrorEnabled(false);
            }
        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.goods_insert_fragment, container, false);
        fab = (FloatingActionButton) getActivity().findViewById(R.id.goods_fab);
        fab.hide();
        Toolbar goodsInsertToolbar = (Toolbar) view.findViewById(R.id.goods_insert_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(goodsInsertToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.add_existing_goods));
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getResources().getString(R.string.title_exist_goods));
        if (goodsInsertToolbar != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
            goodsInsertToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fTrans = getFragmentManager().beginTransaction();
                    fTrans.remove(GoodsExistInsertFragment.this);
                    fTrans.commit();
                }
            });
        }
        getLoaderManager().initLoader(LOADER_G_ID, null, this);
        eNameOfGoods = (EditText) view.findViewById(R.id.e_name_of_the_goods);
        layout_name_of_goods = (TextInputLayout) view.findViewById(R.id.layout_name_of_the_goods);
        eNameOfGoods.setFocusable(false);

        eDescriptionOfGoods = (EditText) view.findViewById(R.id.description_of_the_goods);
        layout_description_of_goods = (TextInputLayout) view.findViewById
                (R.id.layout_description_of_the_goods);
        eDescriptionOfGoods.setFocusable(false);

        sWeightOrPieces = (Spinner)view.findViewById(R.id.s_weight_or_pieces);
        adapterWeightOrPieces =
                ArrayAdapter.createFromResource(getActivity(), R.array.array_weight_or_pieces,
                        R.layout.spinner_item_list);
        sWeightOrPieces.setEnabled(false);

        eWeightOfGoods = (EditText) view.findViewById(R.id.e_weight_of_the_goods);
        layout_weight_of_the_goods = (TextInputLayout) view.findViewById
                (R.id.input_layout_weight_of_the_goods);
        layout_weight_of_the_goods.setVisibility(View.GONE);

        eNumOfGoods = (EditText) view.findViewById(R.id.e_num_of_the_goods);
        layout_number_of_the_goods = (TextInputLayout) view.findViewById
                (R.id.input_layout_number_of_the_goods);
        layout_number_of_the_goods.setVisibility(View.GONE);

        eBasePrice = (EditText) view.findViewById(R.id.base_price);
        layout_base_price_of_the_goods = (TextInputLayout) view.findViewById
                (R.id.input_layout_base_price_of_the_goods);

        eManufacturingDate = (EditText)view.findViewById(R.id.manufacturing_date);
        layout_manufacturing_date = (TextInputLayout) view.findViewById
                (R.id.input_layout_manufacturing_date);

        eApplicationTime = (EditText)view.findViewById(R.id.application_time);
        layout_application_time = (TextInputLayout) view.findViewById
                (R.id.input_layout_application_time);
        eApplicationTime.setFocusable(false);

        eDeliveryDateToGrocery = (EditText)view.findViewById(R.id.delivery_date_to_grocery);
        layout_delivery_date_to_the_grocery = (TextInputLayout) view.findViewById
                (R.id.input_layout_delivery_date_to_grocery);

        //loader and adapter for populate the sinner suppliers
        sSuppliers = (Spinner)view.findViewById(R.id.s_suppliers);
        adapterSuppliers = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item);
        adapterSuppliers.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        getLoaderManager().initLoader(LOADER_S_ID, null, this);
        sSuppliers.setAdapter(adapterSuppliers);
        // events in a supplier spinner
        sSuppliers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                String selection = sSuppliers.getSelectedItem().toString();
                if (selection.equals(getResources().getString(R.string.new_supplier))) {
                    supplierInsertDialog.show(getActivity().getSupportFragmentManager(), "dialog");
                    supplierInsertDialog.setTargetFragment(GoodsExistInsertFragment.this, 0);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        sManufacturers = (Spinner)view.findViewById(R.id.s_manufacturers);
        adapterManufacturers = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item);
        adapterManufacturers.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        getLoaderManager().initLoader(LOADER_M_ID, null, this);
        sManufacturers.setAdapter(adapterManufacturers);

        // events in a supplier manufacturer
        sManufacturers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                String selection = sManufacturers.getSelectedItem().toString();
                if (selection.equals(getResources().getString(R.string.new_manufacturer))) {
                    manufacturerInsertDialog.show(getActivity().getSupportFragmentManager(), "dialog");
                    manufacturerInsertDialog.setTargetFragment(GoodsExistInsertFragment.this, 0);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {}
        });
        eManufacturingDate = (EditText)view.findViewById(R.id.manufacturing_date);
        eManufacturingDate.setInputType(InputType.TYPE_NULL);
        eManufacturingDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    pickDateToEditText(eManufacturingDate);
                    eManufacturingDate.clearFocus();
                }
            }
        });

        eDeliveryDateToGrocery = (EditText)view.findViewById(R.id.delivery_date_to_grocery);
        eDeliveryDateToGrocery.setInputType(InputType.TYPE_NULL);
        eDeliveryDateToGrocery.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    pickDateToEditText(eDeliveryDateToGrocery);
                    eDeliveryDateToGrocery.clearFocus();
                }
            }
        });
        onTextChangeListener(eWeightOfGoods, layout_weight_of_the_goods);
        onTextChangeListener(eNumOfGoods, layout_number_of_the_goods);
        onTextChangeListener(eBasePrice, layout_base_price_of_the_goods);
        onTextChangeListener(eManufacturingDate, layout_manufacturing_date);
        onTextChangeListener(eDeliveryDateToGrocery, layout_delivery_date_to_the_grocery);
        view.findViewById(R.id.button_save_goods).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        String manufacturingDate;
        String deliveryDateToGrocery;
        double basePrice;
        manufacturingDate = eManufacturingDate.getText().toString();
        deliveryDateToGrocery = eDeliveryDateToGrocery.getText().toString();
        switch (v.getId()) {
            case R.id.button_save_goods:
                if (sWeightOrPieces.getSelectedItem().toString().equals(getResources().getString(R.string.weight))) {
                    try {
                        weightOfGoods = Double.valueOf(eWeightOfGoods.getText().toString());
                        piecesOrGrams = 0;
                    } catch (NumberFormatException e) {
                        layout_weight_of_the_goods.setErrorEnabled(true);
                        layout_weight_of_the_goods.setError(getResources().getString
                                (R.string.enter_weight_of_goods));
                        eWeightOfGoods.requestFocus();
                        eWeightOfGoods.setError(getResources().getString(R.string.field_is_empty));
                        return;
                    }
                }
                if (sWeightOrPieces.getSelectedItem().toString().equals(getResources().getString(R.string.pieces))) {
                    try {
                        numOfGoods = Integer.valueOf(eNumOfGoods.getText().toString());
                        piecesOrGrams = 1;
                    } catch (NumberFormatException e) {
                        layout_number_of_the_goods.setErrorEnabled(true);
                        layout_number_of_the_goods.setError(getResources().getString
                                (R.string.enter_weight_of_goods));
                        eNumOfGoods.requestFocus();
                        eNumOfGoods.setError(getResources().getString(R.string.field_is_empty));
                        return;
                    }
                }
                try {
                    basePrice = Double.valueOf(eBasePrice.getText().toString());
                }
                catch (NumberFormatException e) {
                    layout_base_price_of_the_goods.setErrorEnabled(true);
                    layout_base_price_of_the_goods.setError(getResources().getString
                            (R.string.enter_base_price_of_goods));
                    eBasePrice.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }
                if (sSuppliers.getSelectedItem().toString().equals(getResources().getString(R.string.choose_supplier))){
                    Toast.makeText(getActivity(), getResources().getString
                            (R.string.please_choose_supplier), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (sManufacturers.getSelectedItem().toString().equals((getResources().getString(R.string.choose_manufacturer)))){
                    Toast.makeText(getActivity(), getResources().getString
                            (R.string.please_choose_manufacturer), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (manufacturingDate.matches("")) {
                    layout_manufacturing_date.setErrorEnabled(true);
                    layout_manufacturing_date.setError(getResources().getString
                            (R.string.enter_manufacturing_date));
                    eManufacturingDate.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }
                if (deliveryDateToGrocery.matches("")) {
                    layout_delivery_date_to_the_grocery.setErrorEnabled(true);
                    layout_delivery_date_to_the_grocery.setError(getResources().getString
                            (R.string.enter_delivery_date_to_grocery));
                    eDeliveryDateToGrocery.setError(getResources().
                            getString(R.string.field_is_empty));
                    return;
                }
                String projectionS [] = {DataBaseHelper.SUPPLIER_ID};
                String selectionS = DataBaseHelper.NAME_OF_SUPPLIER+"=?";
                String selectionArgsS [] = {sSuppliers.getSelectedItem().toString()};
                Cursor cursorS = cr.query(DataBaseContentProvider.SUPPLIER_CONTENT_URI,
                        projectionS, selectionS, selectionArgsS, null);
                if (cursorS != null && cursorS.moveToFirst()) {
                    supplier_id = cursorS.getLong(cursorS.getColumnIndexOrThrow(DataBaseHelper.SUPPLIER_ID));
                }

                String projectionM [] = {DataBaseHelper.MANUFACTURER_ID};
                String selectionM = DataBaseHelper.NAME_OF_MANUFACTURER+"=?";
                String selectionArgsM [] = {sManufacturers.getSelectedItem().toString()};
                Cursor cursorM = cr.query(DataBaseContentProvider.MANUFACTURER_CONTENT_URI,
                        projectionM, selectionM, selectionArgsM, null);
                if (cursorM != null && cursorM.moveToFirst()) {
                    manufacturer_id = cursorM.getLong(cursorM.getColumnIndexOrThrow(DataBaseHelper.MANUFACTURER_ID));
                }

                cv.put(DataBaseHelper.GOODS_ID, goods_id);
                cv.put(DataBaseHelper.WEIGHT_OF_GOODS, weightOfGoods);
                cv.put(DataBaseHelper.NUMBER_OF_GOODS, numOfGoods);
                cv.put(DataBaseHelper.BASE_PRICE, basePrice);
                cv.put(DataBaseHelper.SUPPLIER_ID, supplier_id);
                cv.put(DataBaseHelper.MANUFACTURER_ID, manufacturer_id);
                cv.put(DataBaseHelper.MANUFACTURING_DATE, manufacturingDate);
                cv.put(DataBaseHelper.DELIVERY_DATE_TO_GROCERY, deliveryDateToGrocery);
                Uri InsertUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, grocery_id);
                try {
                    cr.insert(InsertUri, cv);
                }catch (SQLiteConstraintException e) {
                    layout_name_of_goods.setErrorEnabled(true);
                    layout_name_of_goods.setError(getResources().getString
                            (R.string.goods_is_already_exist));
                    return;
                }
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                fTrans.remove(this);
                fTrans.commit();
                break;
        }
    }
    @Override
    public void onStop() {
        super.onStop();
        eNameOfGoods.setError(null);
        eDescriptionOfGoods.setError(null);
        eBasePrice.setError(null);
        eManufacturingDate.setError(null);
        eApplicationTime.setError(null);
        eDeliveryDateToGrocery.setError(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.drawable.ic_ab_back_holo_light){
            return false;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_G_ID:
                String[] projectionG = new String[]{
                        DataBaseHelper.GOODS_ID + " AS _id",
                        DataBaseHelper.NAME_OF_GOODS,
                        DataBaseHelper.DESCRIPTION_OF_GOODS,
                        DataBaseHelper.PIECES_OR_GRAMS,
                        DataBaseHelper.APPLICATION_TIME
                };
                String selectionG = DataBaseHelper.GOODS_ID + "=?";
                String selectionArgsG [] = {String.valueOf(goods_id)};
                return new InsertExistGoodsLoader(getContext(), projectionG, selectionG,selectionArgsG);

            case LOADER_S_ID:
                String[] projectionS = new String[]{
                        DataBaseHelper.SUPPLIER_ID + " AS _id",
                        DataBaseHelper.NAME_OF_SUPPLIER
                };
                return new SupplierLoader(getContext(), projectionS, null, null);
            case LOADER_M_ID:
                String[] projectionM = new String[]{
                        DataBaseHelper.MANUFACTURER_ID + " AS _id",
                        DataBaseHelper.NAME_OF_MANUFACTURER
                };
                return new ManufacturerLoader(getContext(), projectionM, null, null);
            default: return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case LOADER_G_ID:
                if (cursor!=null && cursor.moveToFirst()){
                    eNameOfGoods.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GOODS)));
                    eDescriptionOfGoods.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.DESCRIPTION_OF_GOODS)));
                    piecesOrGrams = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.PIECES_OR_GRAMS));
                    if (piecesOrGrams == 0){
                        sWeightOrPieces.setSelection(adapterWeightOrPieces.getPosition(getResources().getString(R.string.weight)));
                        layout_weight_of_the_goods.setVisibility(View.VISIBLE);
                        //eWeightOfGoods.requestFocus();
                    }
                    else {
                        sWeightOrPieces.setSelection(adapterWeightOrPieces.getPosition(getResources().getString(R.string.pieces)));
                        layout_number_of_the_goods.setVisibility(View.VISIBLE);
                        //eNumOfGoods.requestFocus();
                    }
                    eApplicationTime.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.APPLICATION_TIME)));
                }
                break;
            case LOADER_M_ID:
                adapterManufacturers.clear();
                adapterManufacturers.add(getResources().getString(R.string.choose_manufacturer));
                adapterManufacturers.add(getResources().getString(R.string.new_manufacturer));
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        String nameOfManufacturer = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_MANUFACTURER));
                        adapterManufacturers.add(nameOfManufacturer);
                    } while (cursor.moveToNext());
                }
                sManufacturers.setSelection(adapterManufacturers.getPosition(nameOfManufacturer));
                break;

            case LOADER_S_ID:
                adapterSuppliers.clear();
                adapterSuppliers.add(getResources().getString(R.string.choose_supplier));
                adapterSuppliers.add(getResources().getString(R.string.new_supplier));
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        String nameOfSupplier = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_SUPPLIER));
                        adapterSuppliers.add(nameOfSupplier);
                    } while (cursor.moveToNext());
                }
                sSuppliers.setSelection(adapterSuppliers.getPosition(nameOfSupplier));
                break;



        }
    }
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {}

    @Override
    public void onFinishSupplierInsert(String nameOfSupplier) {
        this.nameOfSupplier = nameOfSupplier;
        if (nameOfSupplier.equals("")) {
            sSuppliers.setSelection(adapterSuppliers.getPosition(getResources().getString(R.string.choose_supplier)));
        }
    }
    @Override
    public void onFinishManufacturerInsert(String nameOfManufacturer) {
        this.nameOfManufacturer = nameOfManufacturer;
        if (nameOfManufacturer.equals("")) {
            sManufacturers.setSelection(adapterManufacturers.getPosition(getResources().getString(R.string.choose_manufacturer)));
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        fab.show();
    }
}
