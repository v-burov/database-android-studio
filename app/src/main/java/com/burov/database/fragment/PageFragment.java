package com.burov.database.fragment;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.support.v4.content.Loader;
import com.burov.database.Service.LoadGroceriesFromServer;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.R;
import com.burov.database.activity.ListOfGoodsActivity;
import com.burov.database.adapter.GroceryCursorAdapter;
import com.burov.database.adapter.SampleFragmentPagerAdapter;
import com.burov.database.loader.GroceryLoader;
import com.burov.database.provider.DataBaseContentProvider;


/**
 * Created by home on 30.07.2015.
 */
public class PageFragment extends Fragment implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor>{
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private static final String ARG_PAGE = "NUM_OF_THE_TAB";
    private static final String GROCERY_ID = "groceryId";
    private int mPage;
    ListView list;
    FloatingActionButton fab = null;
    GroceryCursorAdapter adapter;
    private static final int LOADER_ID = 1;
    private DialogFragment dialog;
    private long idOfGrocery;
    private final String LOG_TAG = "myLogs";

    public static PageFragment newInstance(SampleFragmentPagerAdapter.TypeOfGroceryTab typeOfGroceryTab) {
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        switch (typeOfGroceryTab) {
            case SupermarketyTab:
                args.putInt(ARG_PAGE, 0);
                break;
            case GastronomyTab:
                args.putInt(ARG_PAGE, 1);
                break;
            case KioskiTab:
                args.putInt(ARG_PAGE, 2);
                break;
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        dialog = new GroceryInsertDialogFragment().newInstanceInsertGrocery(mPage);
    }

   @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       final View view = inflater.inflate(R.layout.fragment_page, container, false);
       mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
       mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
           @Override
           public void onRefresh() {
               // start intent service here
               Intent intentIntentService = new Intent(getActivity(), LoadGroceriesFromServer.class);
               getActivity().startService(intentIntentService);
               mSwipeRefreshLayout.setRefreshing(false);
           }
       });

       list = (ListView) view.findViewById(R.id.lvFragment);
       fab = (FloatingActionButton) view.findViewById(R.id.grocery_fab);
       fab.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               dialog.show(getActivity().getSupportFragmentManager(), "dialog");
           }
       });
       getLoaderManager().initLoader(LOADER_ID, null, this);
       adapter = new GroceryCursorAdapter(getActivity(), null, 0);
       list.setAdapter(adapter);
       registerForContextMenu(list);
       list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view,
                                   int position, long id) {
               Intent i = new Intent(view.getContext(), ListOfGoodsActivity.class);
               idOfGrocery = adapter.getItemId(position);
               i.putExtra(GROCERY_ID, idOfGrocery);
               startActivity(i);
           }
       });
       return view;
   }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_of_the_listview, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        DialogFragment dialogEditGrocery;
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        ContentResolver cr = getActivity().getContentResolver();
        Uri singleGroceryUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, info.id);
        long idOfGrocery = info.id;
        String selection = DataBaseHelper.GROCERY_ID + " = ?";

        switch(item.getItemId()) {
            case R.id.edit:
                dialogEditGrocery = new GroceryEditDialogFragment().newInstanceEditGrocery(idOfGrocery);
                dialogEditGrocery.show(getActivity().getSupportFragmentManager(), "dialog");
                return true;
            case R.id.delete:
                cr.delete(singleGroceryUri, selection, new String[]{String.valueOf(idOfGrocery)});
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        mPage = getArguments().getInt(ARG_PAGE);
        String selection = DataBaseHelper.TYPE_OF_GROCERY + "=?";
        String [] selectionArgsForSupermarkety = new String[] {String.valueOf(0)};
        String [] selectionArgsForGastronomy = new String[] {String.valueOf(1)};
        String [] selectionArgsForKioski = new String[] {String.valueOf(2)};
        String [] projection = new String []{
            DataBaseHelper.GROCERY_ID + " AS _id",
            DataBaseHelper.TYPE_OF_GROCERY,
            DataBaseHelper.NAME_OF_GROCERY,
            DataBaseHelper.ADDRESS
        };

        switch (mPage){
            case 0:
                return new GroceryLoader(getContext(),projection, selection, selectionArgsForSupermarkety);
            case 1:
                return new GroceryLoader(getContext(),projection, selection, selectionArgsForGastronomy);
            case 2:
                return new GroceryLoader(getContext(),projection, selection, selectionArgsForKioski);
            default:
        }
        return  null;
    }
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        adapter.swapCursor(cursor);
    }
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}