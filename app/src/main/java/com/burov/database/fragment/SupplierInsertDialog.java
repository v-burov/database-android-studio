package com.burov.database.fragment;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.burov.database.R;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.provider.DataBaseContentProvider;

/**
 * Created by Vitaliy on 06-Dec-15.
 */
public class SupplierInsertDialog extends DialogFragment implements View.OnClickListener {

    EditText eNameOfSupplier;
    EditText eDescriptionOfSupplier;
    final String LOG_TAG = "myLogs";
    ContentValues cv;
    ContentResolver cr;
    private String nameOfSupplier = "";
    TextInputLayout input_layout_name_of_supplier, input_layout_description_of_supplier;
    SupplierInsertListener fragment;
    private static final String NAME_OF_SUPPLIER = "nameOfSupplier";

    private static final String DESCRIPTION_OF_SUPPLIER = "descriptionOfSupplier";

    public interface SupplierInsertListener {
        void onFinishSupplierInsert(String nameOfSupplier);
    }

    private void onTextChangeListener(final EditText e, final TextInputLayout textInputLayout){
        e.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0)
                    e.setError(null);
                textInputLayout.setErrorEnabled(false);
            }
        });
    }

    public static SupplierInsertDialog newInstanceSupplierInsert() {
        SupplierInsertDialog fragment = new SupplierInsertDialog();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cr = getActivity().getContentResolver();
        fragment = (SupplierInsertListener) getTargetFragment();
        cv = new ContentValues();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().setTitle(getResources().getString(R.string.add_supplier));

        View view = inflater.inflate(R.layout.supplier_insert_dialog, container, false);

        eNameOfSupplier = (EditText)view.findViewById(R.id.e_name_of_the_supplier);
        eDescriptionOfSupplier = (EditText)view.findViewById(R.id.e_description_of_the_supplier);

        eNameOfSupplier.setOnClickListener(this);
        eDescriptionOfSupplier.setOnClickListener(this);

        input_layout_name_of_supplier = (TextInputLayout) view.findViewById(R.id.input_layout_name_of_supplier);
        input_layout_description_of_supplier = (TextInputLayout) view.findViewById(R.id.input_layout_description_of_supplier);

        onTextChangeListener(eNameOfSupplier, input_layout_name_of_supplier);
        onTextChangeListener(eDescriptionOfSupplier, input_layout_description_of_supplier);

        view.findViewById(R.id.saveSupplier).setOnClickListener(this);;
        view.findViewById(R.id.cancelSupplier).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        String descriptionOfSupplier;
        switch (v.getId()) {
            case R.id.saveSupplier:
                nameOfSupplier = eNameOfSupplier.getText().toString();
                descriptionOfSupplier = eDescriptionOfSupplier.getText().toString();
                if (nameOfSupplier.matches("")){
                    input_layout_name_of_supplier.setErrorEnabled(true);
                    input_layout_name_of_supplier.setError(getResources().getString(R.string.enter_name_of_supplier));
                    eNameOfSupplier.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }
                if(descriptionOfSupplier.matches("")) {
                    input_layout_description_of_supplier.setErrorEnabled(true);
                    input_layout_description_of_supplier.setError(getResources().getString(R.string.enter_description_of_supplier));
                    eDescriptionOfSupplier.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }

                cv.put(DataBaseHelper.NAME_OF_SUPPLIER, nameOfSupplier);
                cv.put(DataBaseHelper.DESCRIPTION_OF_SUPPLIER, descriptionOfSupplier);
                try {
                    cr.insert(DataBaseContentProvider.SUPPLIER_CONTENT_URI, cv);
                }
                catch (SQLiteConstraintException e) {
                    input_layout_name_of_supplier.setErrorEnabled(true);
                    input_layout_name_of_supplier.setError(getResources().getString
                            (R.string.supplier_is_already_exist));
                    return;
                }
                fragment.onFinishSupplierInsert(nameOfSupplier);
                dismiss();
                break;
            case R.id.cancelSupplier:
                dismiss();
                break;
        }
    }
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        eNameOfSupplier.setError(null);
        eDescriptionOfSupplier.setError(null);
        if (getActivity()!=null) {
            fragment.onFinishSupplierInsert(nameOfSupplier);
        }
    }
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}
