package com.burov.database.fragment;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.burov.database.R;
import com.burov.database.loader.LoaderOfAllGoods;
import com.burov.database.model.DataBaseHelper;
/**
 * Created by Vitaliy on 22-Dec-15.
 */
public class GoodsChoiceDialog extends DialogFragment implements View.OnClickListener,
        android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor>{
    private String nameOfGoods;
    private final String LOG_TAG = "myLogs";
    ListView listOfGoods;
    TextView addNewGoods;
    SimpleCursorAdapter goodsAdapter;
    private long idOfGoods;
    private long idOfGrocery = 0L;
    private static final String GROCERY_ID = "GROCERY_ID";

    GoodsInsertFragment goodsInsertFragment;
    FragmentTransaction fTrans;

    public static GoodsChoiceDialog newGoodsChoiceDialog(long idOfGrocery) {
        GoodsChoiceDialog fragment = new GoodsChoiceDialog();
        Bundle args = new Bundle();
        args.putLong(GROCERY_ID, idOfGrocery);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);
        idOfGrocery = getArguments().getLong(GROCERY_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(getResources().getString(R.string.choose_goods));
        View view = inflater.inflate(R.layout.selection_dialog, container, false);

        listOfGoods = (ListView) view.findViewById(R.id.list_of_exist_goods);
        addNewGoods = (TextView) view.findViewById(R.id.new_goods);
        addNewGoods.setOnClickListener(this);

        goodsAdapter = new SimpleCursorAdapter(getActivity(), android.R.layout.simple_list_item_1,
                null,new String[] {DataBaseHelper.NAME_OF_GOODS},
                new int[]{android.R.id.text1},0);
        listOfGoods.setAdapter(goodsAdapter);
        listOfGoods.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                idOfGoods = goodsAdapter.getItemId(position);
                GoodsExistInsertFragment fragment = GoodsExistInsertFragment.newGoodsExistInsertFragment(idOfGrocery, idOfGoods);
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.fragCont, fragment);
                fTrans.commit();
                dismiss();

            }
        });
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.new_goods:
                goodsInsertFragment = new GoodsInsertFragment().newInstanceInsertGoods(idOfGrocery);
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.fragCont, goodsInsertFragment);
                fTrans.commit();
                dismiss();
              break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new LoaderOfAllGoods(getContext());
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        goodsAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

    }
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}
