package com.burov.database.fragment;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.R;
import com.burov.database.provider.DataBaseContentProvider;

/**
 * Created by home on 10.10.2015.
 */
public class GroceryInsertDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String NAME_OF_GROCERY = "nameOfGrocery";
    private static final String ADDRESS_OF_GROCERY = "addressOfGrocery";
    private static final String MARKUP = "markup";
    private static final String TYPE_OF_GROCERY = "typeOfGrocery";

    EditText eNameOfGrocery;
    EditText eAddressOfGrocery;
    EditText eMarkupOfGrocery;
    private final String LOG_TAG = "myLogs";
    ContentValues cv;
    ContentResolver cr;
    private static final String NUM_OF_THE_TAB = "NUM_OF_THE_TAB";
    private int typeOfGrocery = 0;
    TextInputLayout input_layout_name_of_grocery, input_layout_address_of_grocery, input_layout_markup;

    public static GroceryInsertDialogFragment newInstanceInsertGrocery(int typeOfGrocery) {

        GroceryInsertDialogFragment fragment = new GroceryInsertDialogFragment();
        Bundle args = new Bundle();
        args.putInt(NUM_OF_THE_TAB, typeOfGrocery);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cr = getActivity().getContentResolver();
        typeOfGrocery = getArguments().getInt(NUM_OF_THE_TAB);
        cv = new ContentValues();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().setTitle(getResources().getString(R.string.add_grocery));
        View view = inflater.inflate(R.layout.grocery_insert_dialog_fragment, container, false);
        eNameOfGrocery = (EditText)view.findViewById(R.id.e_name_of_the_grocery);
        eAddressOfGrocery = (EditText)view.findViewById(R.id.address_of_the_grocery);
        eMarkupOfGrocery = (EditText)view.findViewById(R.id.markup_of_the_grocery);
        eNameOfGrocery.setOnClickListener(this);
        eAddressOfGrocery.setOnClickListener(this);
        eMarkupOfGrocery.setOnClickListener(this);

        input_layout_name_of_grocery = (TextInputLayout) view.findViewById(R.id.input_layout_name_of_grocery);
        input_layout_address_of_grocery = (TextInputLayout) view.findViewById(R.id.input_layout_address_of_grocery);
        input_layout_markup = (TextInputLayout) view.findViewById(R.id.input_layout_markup);

        onTextChangeListener(eNameOfGrocery, input_layout_name_of_grocery);
        onTextChangeListener(eAddressOfGrocery, input_layout_address_of_grocery);
        onTextChangeListener(eMarkupOfGrocery, input_layout_markup);

        view.findViewById(R.id.save).setOnClickListener(this);;
        view.findViewById(R.id.cancel).setOnClickListener(this);
        return view;
    }
    private void onTextChangeListener(final EditText e, final TextInputLayout textInputLayout){
        e.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0)
                    e.setError(null);
                textInputLayout.setErrorEnabled(false);
            }
        });
    }

    @Override
    public void onClick(View v) {
        String name;
        String address;
        double markup;

        name = eNameOfGrocery.getText().toString();
        address = eAddressOfGrocery.getText().toString();

        switch (v.getId()) {
            case R.id.save:
                if (name.matches("")){
                    input_layout_name_of_grocery.setErrorEnabled(true);
                    input_layout_name_of_grocery.setError(getResources().getString(R.string.enter_name_of_grocery));
                    eNameOfGrocery.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }
                if(address.matches("")) {
                    input_layout_address_of_grocery.setErrorEnabled(true);
                    input_layout_address_of_grocery.setError(getResources().getString(R.string.enter_address_of_grocery));
                    eAddressOfGrocery.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }

                try {
                    markup = Double.parseDouble(eMarkupOfGrocery.getText().toString());
                }
                catch (NumberFormatException e) {
                    input_layout_markup.setErrorEnabled(true);
                    input_layout_markup.setError(getResources().getString(R.string.enter_numeric_value));
                    eMarkupOfGrocery.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }

                cv.clear();
                cv.put(DataBaseHelper.TYPE_OF_GROCERY, typeOfGrocery);
                cv.put(DataBaseHelper.NAME_OF_GROCERY, name);
                cv.put(DataBaseHelper.ADDRESS, address);
                cv.put(DataBaseHelper.MARKUP, markup);
                try {
                    cr.insert(DataBaseContentProvider.GROCERY_CONTENT_URI, cv);
                }catch (SQLiteConstraintException e) {
                    input_layout_name_of_grocery.setErrorEnabled(true);
                    input_layout_name_of_grocery.setError(getResources().getString
                            (R.string.grocery_is_already_exist));
                    return;
                }
                dismiss();
                break;
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        eNameOfGrocery.setError(null);
        eAddressOfGrocery.setError(null);
        eMarkupOfGrocery.setError(null);
        eNameOfGrocery.setText(null);
        eAddressOfGrocery.setText(null);
        eMarkupOfGrocery.setText(null);
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

    }
}
