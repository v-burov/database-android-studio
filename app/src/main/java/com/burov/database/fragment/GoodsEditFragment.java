package com.burov.database.fragment;

import android.app.DatePickerDialog;


import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.support.v4.content.Loader;
import com.burov.database.R;
import com.burov.database.loader.EditGoodsLoader;
import com.burov.database.loader.ManufacturerLoader;
import com.burov.database.loader.SupplierLoader;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.provider.DataBaseContentProvider;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Vitaliy on 15-Nov-15.
 */
public class GoodsEditFragment extends Fragment implements View.OnClickListener,
        android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor>,
        SupplierInsertDialog.SupplierInsertListener,
        ManufacturerInsertDialog.ManufacturerInsertListener{

    EditText eNameOfGoods;
    EditText eDescriptionOfGoods;
    Spinner sWeightOrPieces;
    EditText eWeightOfGoods;
    EditText eNumOfGoods;
    EditText eBasePrice;
    Spinner sSuppliers;
    Spinner sManufacturers;
    ArrayAdapter<String> adapterSuppliers;
    ArrayAdapter<String> adapterManufacturers;
    EditText eManufacturingDate;
    EditText eApplicationTime;
    EditText eDeliveryDateToGrocery;
    private double weightOfGoods;
    private int numOfGoods;
    private String nameOfSupplier;
    private String selectedWeightOrPieces;
    private String nameOfManufacturer;
    private String manufacturingDate;
    private long manufacturer_id;
    private long supplier_id;
    private long gmGoods_id;
    private long goodsManufacturing_id;
    private long gsGoodsManufacturing_id;
    private long goodsSupplier_id;
    private long ggGrocery_id;
    private long ggGoodsSupplier_id;
    private long goodsGrocery_id;

    ContentValues cv = null;
    ContentResolver cr = null;
    FragmentTransaction fTrans;
    private SimpleDateFormat dateFormatter;
    private static final int LOADER_ID = 4;
    private static final int LOADER_S_ID = 5;
    private static final int LOADER_M_ID = 6;
    ArrayAdapter<?> adapter;
    FloatingActionButton fab;

    TextInputLayout layout_name_of_the_goods, layout_description_of_the_goods,
            layout_weight_of_the_goods, layout_number_of_the_goods,
            layout_base_price_of_the_goods, layout_manufacturing_date,
            layout_application_time, layout_date_delivery_to_the_grocery;

    public static final String ID_OF_THE_GOODS = "ID_OF_THE_GOODS";
    public static final String ID_OF_THE_GROCERY = "ID_OF_THE_GROCERY";
    SupplierInsertDialog supplierInsertDialog;
    ManufacturerInsertDialog manufacturerInsertDialog;
    private static final String NAME_OF_SUPPLIER = "nameOfSupplier";
    private static final String NAME_OF_MANUFACTURER = "nameOfManufacturer";
    private static final String WEIGHT_OR_PIECES = "weightOrPieces";
    private static final String GOODS_MANUFACTURING_ID = "goodsManufacturing_id";
    private static final String GM_GOODS_ID = "gmGoods_id";
    private static final String GOODS_SUPPLIER_ID = "goodsSupplier_id";
    private static final String GS_GOODS_MANUFACTURING_ID = "gsGoodsManufacturing_id";
    private static final String GG_GROCERY_ID = "ggGrocery_id";
    private static final String GG_GOODS_SUPPLIER_ID = "ggGoodsSupplier_id";

    public static GoodsEditFragment newInstanceEditGoods(long idOfGoods, long idOfGrocery) {
        GoodsEditFragment fragment = new GoodsEditFragment();
        Bundle args = new Bundle();
        args.putLong(ID_OF_THE_GOODS, idOfGoods);
        args.putLong(ID_OF_THE_GROCERY, idOfGrocery);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cr = getActivity().getContentResolver();
        goodsGrocery_id = getArguments().getLong(ID_OF_THE_GOODS);
        cv = new ContentValues();
        supplierInsertDialog = new SupplierInsertDialog().newInstanceSupplierInsert();
        manufacturerInsertDialog = new ManufacturerInsertDialog().newInstanceManufacturerInsert();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        selectedWeightOrPieces = sWeightOrPieces.getSelectedItem().toString();
        nameOfSupplier = sSuppliers.getSelectedItem().toString();
        nameOfManufacturer = sManufacturers.getSelectedItem().toString();
        outState.putString(NAME_OF_SUPPLIER, nameOfSupplier);
        outState.putString(NAME_OF_MANUFACTURER, nameOfManufacturer);
        outState.putString(WEIGHT_OR_PIECES, selectedWeightOrPieces);
        outState.putLong(GOODS_MANUFACTURING_ID, gsGoodsManufacturing_id);
        outState.putLong(GM_GOODS_ID, gmGoods_id);
        outState.putLong(GOODS_SUPPLIER_ID, goodsSupplier_id);
        outState.putLong(GS_GOODS_MANUFACTURING_ID, gsGoodsManufacturing_id);
        outState.putLong(GG_GROCERY_ID, ggGrocery_id);
        outState.putLong(GG_GOODS_SUPPLIER_ID, ggGoodsSupplier_id);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState!=null) {
            nameOfSupplier = savedInstanceState.getString(NAME_OF_SUPPLIER);
            if (nameOfSupplier.equals(getResources().getString(R.string.new_supplier))) {
                nameOfSupplier = null;
            }
            nameOfManufacturer = savedInstanceState.getString(NAME_OF_MANUFACTURER);
            if (nameOfManufacturer.equals(getResources().getString(R.string.new_manufacturer))) {
                nameOfManufacturer = null;
            }
            sSuppliers.setSelection(adapterSuppliers.getPosition(nameOfSupplier));
            sManufacturers.setSelection(adapterManufacturers.getPosition(nameOfManufacturer));
            selectedWeightOrPieces = savedInstanceState.getString(WEIGHT_OR_PIECES);
            getLoaderManager().initLoader(LOADER_M_ID, null, this);
            getLoaderManager().initLoader(LOADER_S_ID, null, this);
            if (selectedWeightOrPieces.equals(getResources().getString(R.string.weight))) {
                sWeightOrPieces.setSelection(1);
                eNumOfGoods.setVisibility(View.GONE);
            }
            else if (selectedWeightOrPieces.equals(getResources().getString(R.string.weight))){
                sWeightOrPieces.setSelection(2);
                eWeightOfGoods.setVisibility(View.GONE);
            }
            goodsManufacturing_id = savedInstanceState.getLong(GOODS_MANUFACTURING_ID);
            gmGoods_id = savedInstanceState.getLong(GM_GOODS_ID);
            goodsSupplier_id = savedInstanceState.getLong(GOODS_SUPPLIER_ID);
            gsGoodsManufacturing_id = savedInstanceState.getLong(GS_GOODS_MANUFACTURING_ID);
            ggGrocery_id = savedInstanceState.getLong(GG_GROCERY_ID);
            ggGoodsSupplier_id = savedInstanceState.getLong(GG_GOODS_SUPPLIER_ID);
        }
        else {
            getLoaderManager().initLoader(LOADER_ID, null, this);
        }
    }


    private void onTextChangeListener(final EditText e, final TextInputLayout textInputLayout){
        e.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0)
                    e.setError(null);
                textInputLayout.setErrorEnabled(false);
            }
        });
    }
    private void pickDateToEditText (final EditText e) {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        final Calendar mCalendar=Calendar.getInstance();
        DatePickerDialog mDatePicker=new DatePickerDialog
                (getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedYear, int selectedMonth, int selectedDay) {
                        mCalendar.set(selectedYear, selectedMonth, selectedDay);
                        e.setText(dateFormatter.format(mCalendar.getTime()));
                    }
                },mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
        mDatePicker.show();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.goods_edit_fragment, container, false);
        fab = (FloatingActionButton) getActivity().findViewById(R.id.goods_fab);
        fab.hide();
        Toolbar goodsEditToolbar = (Toolbar) view.findViewById(R.id.goods_insert_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(goodsEditToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.edit_goods));
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getResources().getString(R.string.title_goods));

        if (goodsEditToolbar != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            goodsEditToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fTrans = getFragmentManager().beginTransaction();
                    fTrans.remove(GoodsEditFragment.this);
                    fTrans.commit();
                }
            });
        }
        //init the loader to load a data to the editText

        eNameOfGoods = (EditText)view.findViewById(R.id.name_of_the_goods);
        eNameOfGoods.setFocusable(false);
        eDescriptionOfGoods = (EditText)view.findViewById(R.id.description_of_the_goods);
        eDescriptionOfGoods.setFocusable(false);
        sWeightOrPieces = (Spinner)view.findViewById(R.id.s_weight_or_pieces);
        sWeightOrPieces.setEnabled(false);
        adapter = ArrayAdapter.createFromResource(getActivity(), R.array.array_weight_or_pieces,
                        R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sWeightOrPieces.setAdapter(adapter);

        eWeightOfGoods = (EditText)view.findViewById(R.id.e_weight_of_the_goods);
        eNumOfGoods = (EditText)view.findViewById(R.id.e_num_of_the_goods);

        eBasePrice = (EditText)view.findViewById(R.id.base_price);

        sSuppliers = (Spinner) view.findViewById(R.id.s_suppliers_edit);
        adapterSuppliers = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item);
        adapterSuppliers.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sSuppliers.setAdapter(adapterSuppliers);
        // events in a supplier spinner
        sSuppliers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                String selection = sSuppliers.getSelectedItem().toString();
                if (selection.equals(getResources().getString(R.string.new_supplier))) {
                    supplierInsertDialog.show(getActivity().getSupportFragmentManager(), "dialog");
                    supplierInsertDialog.setTargetFragment(GoodsEditFragment.this, 0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        sManufacturers = (Spinner) view.findViewById(R.id.s_manufacturers_edit);
        adapterManufacturers = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item);
        adapterManufacturers.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sManufacturers.setAdapter(adapterManufacturers);
        // events in a supplier manufacturer
        sManufacturers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                String selection = sManufacturers.getSelectedItem().toString();
                if (selection.equals(getResources().getString(R.string.new_manufacturer))) {
                    manufacturerInsertDialog.show(getActivity().getSupportFragmentManager(), "dialog");
                    manufacturerInsertDialog.setTargetFragment(GoodsEditFragment.this, 0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        eApplicationTime = (EditText)view.findViewById(R.id.application_time_edit);
        eApplicationTime.setEnabled(false);

        eManufacturingDate = (EditText)view.findViewById(R.id.manufacturing_date_edit);
        eManufacturingDate.setInputType(InputType.TYPE_NULL);
        eManufacturingDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    pickDateToEditText(eManufacturingDate);
                    eManufacturingDate.clearFocus();
                }
            }
        });
        eDeliveryDateToGrocery = (EditText)view.findViewById(R.id.delivery_date_to_grocery_edit);
        eDeliveryDateToGrocery.setInputType(InputType.TYPE_NULL);
        eDeliveryDateToGrocery.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    pickDateToEditText(eDeliveryDateToGrocery);
                    eDeliveryDateToGrocery.clearFocus();
                }
            }
        });
        layout_name_of_the_goods = (TextInputLayout) view.findViewById
                (R.id.input_layout_name_of_the_goods);
        layout_description_of_the_goods = (TextInputLayout) view.findViewById
                (R.id.input_layout_description_of_the_goods);
        layout_weight_of_the_goods = (TextInputLayout) view.findViewById
                (R.id.input_layout_weight_of_the_goods);
        layout_number_of_the_goods = (TextInputLayout) view.findViewById
                (R.id.input_layout_number_of_the_goods);
        layout_base_price_of_the_goods = (TextInputLayout) view.findViewById
                (R.id.input_layout_base_price_of_the_goods);
        layout_manufacturing_date = (TextInputLayout) view.findViewById
                (R.id.input_layout_manufacturing_date);
        layout_application_time = (TextInputLayout) view.findViewById
                (R.id.input_layout_application_time);
        layout_date_delivery_to_the_grocery = (TextInputLayout) view.findViewById
                (R.id.input_layout_delivery_date_to_grocery);

        onTextChangeListener(eWeightOfGoods, layout_weight_of_the_goods);
        onTextChangeListener(eNumOfGoods, layout_number_of_the_goods);
        onTextChangeListener(eBasePrice, layout_base_price_of_the_goods);
        onTextChangeListener(eApplicationTime, layout_application_time);

        view.findViewById(R.id.button_save_goods).setOnClickListener(this);
        return view;
    }
    @Override
    public void onClick(View v) {
        manufacturingDate = eManufacturingDate.getText().toString();
        String deliveryDateToGrocery = eDeliveryDateToGrocery.getText().toString();
        selectedWeightOrPieces = sWeightOrPieces.getSelectedItem().toString();
        double basePrice;

        switch (v.getId()) {
            case R.id.button_save_goods:
                if (selectedWeightOrPieces.equals(getResources().getString(R.string.weight)))  {
                    numOfGoods = 0;
                    try {
                        weightOfGoods = Double.valueOf(eWeightOfGoods.getText().toString());
                    }
                    catch (NumberFormatException e) {
                        layout_weight_of_the_goods.setErrorEnabled(true);
                        layout_weight_of_the_goods.setError(getResources().getString
                                (R.string.enter_weight_of_goods));
                        eWeightOfGoods.setError(getResources().getString(R.string.field_is_empty));
                        return;
                    }
                }
                else if (selectedWeightOrPieces.equals(getResources().getString(R.string.pieces))){
                    weightOfGoods = 0d;
                    //eWeightOfGoods.setEnabled(false);
                    try {
                        numOfGoods = Integer.valueOf(eNumOfGoods.getText().toString());
                    }
                    catch (NumberFormatException e) {
                        layout_number_of_the_goods.setErrorEnabled(true);
                        layout_number_of_the_goods.setError(getResources().getString
                                (R.string.enter_number_of_goods));
                        eNumOfGoods.setError(getResources().getString(R.string.field_is_empty));
                        return;
                    }
                }
                try {
                    basePrice = Double.valueOf(eBasePrice.getText().toString());
                }
                catch (NumberFormatException e) {
                    layout_base_price_of_the_goods.setErrorEnabled(true);
                    layout_base_price_of_the_goods.setError(getResources().getString
                            (R.string.enter_base_price_of_goods));
                    eBasePrice.requestFocus();
                    eBasePrice.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }
                if (sSuppliers.getSelectedItem().toString().equals(getResources().getString(R.string.choose_supplier))){
                    Toast.makeText(getActivity(), getResources().getString
                            (R.string.please_choose_supplier), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (sManufacturers.getSelectedItem().toString().equals((getResources().getString(R.string.choose_manufacturer)))){
                    Toast.makeText(getActivity(), getResources().getString
                            (R.string.please_choose_manufacturer), Toast.LENGTH_SHORT).show();
                    return;
                }

                String projectionS [] = {DataBaseHelper.SUPPLIER_ID};
                String selectionS = DataBaseHelper.NAME_OF_SUPPLIER+"=?";
                String selectionArgsS [] = {sSuppliers.getSelectedItem().toString()};
                Cursor cursorS = cr.query(DataBaseContentProvider.SUPPLIER_CONTENT_URI,
                        projectionS, selectionS, selectionArgsS, null);
                if (cursorS != null && cursorS.moveToFirst()) {
                    supplier_id = cursorS.getLong(cursorS.getColumnIndexOrThrow(DataBaseHelper.SUPPLIER_ID));
                    cursorS.close();
                }

                String projectionM [] = {DataBaseHelper.MANUFACTURER_ID};
                String selectionM = DataBaseHelper.NAME_OF_MANUFACTURER+"=?";
                String selectionArgsM [] = {sManufacturers.getSelectedItem().toString()};
                Cursor cursorM = cr.query(DataBaseContentProvider.MANUFACTURER_CONTENT_URI,
                        projectionM, selectionM, selectionArgsM, null);
                if (cursorM != null && cursorM.moveToFirst()) {
                    manufacturer_id = cursorM.getLong(cursorM.getColumnIndexOrThrow(DataBaseHelper.MANUFACTURER_ID));
                    cursorM.close();
                }

                cv.put(DataBaseHelper.GM_MANUFACTURER_ID, manufacturer_id);
                cv.put(DataBaseHelper.GM_GOODS_ID, gmGoods_id);
                cv.put(DataBaseHelper.MANUFACTURING_DATE, manufacturingDate);
                cv.put(DataBaseHelper.GOODS_MANUFACTURING_ID, goodsManufacturing_id);

                cv.put(DataBaseHelper.GS_SUPPLIER_ID, supplier_id);
                cv.put(DataBaseHelper.GS_GOODS_MANUFACTURING_ID, gsGoodsManufacturing_id);
                cv.put(DataBaseHelper.BASE_PRICE, basePrice);
                cv.put(DataBaseHelper.GOODS_SUPPLIER_ID, goodsSupplier_id);

                cv.put(DataBaseHelper.GG_GROCERY_ID, ggGrocery_id);
                cv.put(DataBaseHelper.GG_GOODS_SUPPLIER_ID, ggGoodsSupplier_id);
                cv.put(DataBaseHelper.WEIGHT_OF_GOODS, weightOfGoods);
                cv.put(DataBaseHelper.NUMBER_OF_GOODS, numOfGoods);
                cv.put(DataBaseHelper.DELIVERY_DATE_TO_GROCERY, deliveryDateToGrocery);

                Uri updateUri = ContentUris.withAppendedId(DataBaseContentProvider.GOODS_GROCERY_CONTENT_URI, goodsGrocery_id);
                cr.update(updateUri,cv,null,null);
                fTrans = getFragmentManager().beginTransaction();
                fTrans.remove(this);
                fTrans.commit();
                break;
        }
    }
    @Override
    public void onStop() {
            super.onStop();
            eNameOfGoods.setError(null);
            eDescriptionOfGoods.setError(null);
            sWeightOrPieces.setSelection(0);
            eWeightOfGoods.setError(null);
            eNumOfGoods.setError(null);
            eBasePrice.setError(null);
            eManufacturingDate.setError(null);
            eApplicationTime.setError(null);
            eDeliveryDateToGrocery.setError(null);
            eNameOfGoods.setText("");
            eDescriptionOfGoods.setText("");
            eWeightOfGoods.setText("");
            eNumOfGoods.setText("");
            eBasePrice.setText("");
            eApplicationTime.setText("");
            TextKeyListener.clear(eManufacturingDate.getText());
            TextKeyListener.clear(eDeliveryDateToGrocery.getText());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();

            if (id == R.id.action_settings) {
            return true;
            }
            else if (id == R.drawable.ic_ab_back_holo_light){
            return false;
            }
            return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_ID:
                return new EditGoodsLoader(getContext(),goodsGrocery_id);
            case LOADER_S_ID:
                String[] projectionS = new String[]{
                        DataBaseHelper.SUPPLIER_ID + " AS _id",
                        DataBaseHelper.NAME_OF_SUPPLIER,
                        DataBaseHelper.DESCRIPTION_OF_SUPPLIER
                };

                return new SupplierLoader(getContext(), projectionS, null, null);
            case LOADER_M_ID:
                String[] projectionM = new String[]{
                        DataBaseHelper.MANUFACTURER_ID + " AS _id",
                        DataBaseHelper.NAME_OF_MANUFACTURER,
                        DataBaseHelper.DESCRIPTION_OF_MANUFACTURER
                };
                return new ManufacturerLoader(getContext(), projectionM, null, null);
            default: return null;

        }
    }
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case LOADER_S_ID:
                adapterSuppliers.clear();
                adapterSuppliers.add(getResources().getString(R.string.choose_supplier));
                adapterSuppliers.add(getResources().getString(R.string.new_supplier));

                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        String addNameOfSupplier = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_SUPPLIER));
                        adapterSuppliers.add(addNameOfSupplier);
                    } while (cursor.moveToNext());
                }
                sSuppliers.setSelection(adapterSuppliers.getPosition(nameOfSupplier));
                break;
            case LOADER_M_ID:
                adapterManufacturers.clear();
                adapterManufacturers.add(getResources().getString(R.string.choose_manufacturer));
                adapterManufacturers.add(getResources().getString(R.string.new_manufacturer));
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        String addNameOfManufacturer = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_MANUFACTURER));
                        adapterManufacturers.add(addNameOfManufacturer);
                    } while (cursor.moveToNext());
                }
                sManufacturers.setSelection(adapterManufacturers.getPosition(nameOfManufacturer));
                break;
            case LOADER_ID:
                if (cursor != null && cursor.moveToFirst()) {
                    eNameOfGoods.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GOODS)));
                    eDescriptionOfGoods.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.DESCRIPTION_OF_GOODS)));
                    int pieces = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.PIECES_OR_GRAMS));
                    if (pieces == 0) {
                        eNumOfGoods.setVisibility(View.GONE);
                        sWeightOrPieces.setSelection(1);
                    }
                    else {
                        selectedWeightOrPieces = getResources().getString(R.string.pieces);
                        eWeightOfGoods.setVisibility(View.GONE);
                        sWeightOrPieces.setSelection(2);
                    }
                    sWeightOrPieces.setEnabled(false);
                    eWeightOfGoods.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.WEIGHT_OF_GOODS)));
                    eNumOfGoods.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NUMBER_OF_GOODS)));
                    eBasePrice.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.BASE_PRICE)));

                    eManufacturingDate.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.MANUFACTURING_DATE)));
                    eApplicationTime.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.APPLICATION_TIME)));
                    eDeliveryDateToGrocery.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.DELIVERY_DATE_TO_GROCERY)));

                    manufacturingDate = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.MANUFACTURING_DATE));
                    nameOfSupplier = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_SUPPLIER));
                    nameOfManufacturer = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_MANUFACTURER));

                    goodsManufacturing_id = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_MANUFACTURING_ID));
                    gmGoods_id = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GM_GOODS_ID));

                    goodsSupplier_id = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_SUPPLIER_ID));
                    gsGoodsManufacturing_id = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GS_GOODS_MANUFACTURING_ID));

                    ggGrocery_id = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GG_GROCERY_ID));
                    ggGoodsSupplier_id = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GG_GOODS_SUPPLIER_ID));

                    //init loaders to populate the spinners manufacturers and suppliers
                    getLoaderManager().initLoader(LOADER_S_ID, null, this);
                    getLoaderManager().initLoader(LOADER_M_ID, null, this);
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        loader.deliverResult(null);
    }

    @Override
    public void onFinishSupplierInsert(String nameOfSupplier) {
        this.nameOfSupplier = nameOfSupplier;
        if (nameOfSupplier.equals("")) {
            sSuppliers.setSelection(adapterSuppliers.getPosition(getResources().getString(R.string.choose_supplier)));
        }
    }
    @Override
    public void onFinishManufacturerInsert(String nameOfManufacturer) {
        this.nameOfManufacturer = nameOfManufacturer;
        if (nameOfManufacturer.equals("")) {
            sManufacturers.setSelection(adapterManufacturers.getPosition(getResources().getString(R.string.choose_manufacturer)));
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        fab.show();
    }
}
