package com.burov.database.fragment;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.burov.database.R;
import com.burov.database.loader.EditGroceryLoader;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.provider.DataBaseContentProvider;

/**
 * Created by Vitaliy on 08-Nov-15.
 */
public class GroceryEditDialogFragment  extends DialogFragment implements View.OnClickListener,
        android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor> {
    EditText eNameOfGrocery;
    EditText eAddressOfGrocery;
    EditText eMarkupOfGrocery;
    final String LOG_TAG = "myLogs";
    ContentValues cv = null;
    ContentResolver cr = null;
    private String name = null;
    private String address = null;
    private double markup;
    private static final String GROCERY_ID = "GROCERY_ID";
    private int typeOfGrocery;
    TextInputLayout input_layout_name_of_grocery, input_layout_address_of_grocery, input_layout_markup;
    private String selection;
    private String [] selectionArgs = null;
    private String [] projection;
    private static final int LOADER_ID = 3;
    private long idOfGrocery;
    private static final String NAME_OF_GROCERY = "nameOfGrocery";
    private static final String ADDRESS_OF_GROCERY = "addressOfGrocery";
    private static final String MARKUP = "markup";
    private static final String TYPE_OF_GROCERY = "typeOfGrocery";

    public static GroceryEditDialogFragment newInstanceEditGrocery(long idOfGrocery) {
        GroceryEditDialogFragment fragment = new GroceryEditDialogFragment();
        Bundle args = new Bundle();
        args.putLong(GROCERY_ID, idOfGrocery);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cv = new ContentValues();
        cr = getActivity().getContentResolver();
        idOfGrocery = getArguments().getLong(GROCERY_ID);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            //idOfGrocery = getArguments().getLong(GROCERY_ID);
            //typeOfGrocery = savedInstanceState.getInt(TYPE_OF_GROCERY);
            name = savedInstanceState.getString(NAME_OF_GROCERY);
            address = savedInstanceState.getString(ADDRESS_OF_GROCERY);
            markup = savedInstanceState.getDouble(MARKUP);
        }
        else {
            getLoaderManager().initLoader(LOADER_ID, null, this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        name = eNameOfGrocery.getText().toString();
        outState.putString(NAME_OF_GROCERY, name);
        address = eAddressOfGrocery.getText().toString();
        outState.putString(ADDRESS_OF_GROCERY, address);
        //outState.putInt(TYPE_OF_GROCERY, typeOfGrocery);
        try {
            markup = Double.valueOf(eMarkupOfGrocery.getText().toString());
        }
        catch (NumberFormatException ex) {markup = 0;}
        outState.putDouble(MARKUP, markup);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().setTitle(getResources().getString(R.string.edit_grocery));

        View view = inflater.inflate(R.layout.grocery_insert_dialog_fragment, container, false);

        eNameOfGrocery = (EditText) view.findViewById(R.id.e_name_of_the_grocery);
        eNameOfGrocery.setEnabled(false);
        eAddressOfGrocery = (EditText) view.findViewById(R.id.address_of_the_grocery);
        eMarkupOfGrocery = (EditText) view.findViewById(R.id.markup_of_the_grocery);

        eNameOfGrocery.setOnClickListener(this);
        eAddressOfGrocery.setOnClickListener(this);
        eMarkupOfGrocery.setOnClickListener(this);

        input_layout_name_of_grocery = (TextInputLayout) view.findViewById(R.id.input_layout_name_of_grocery);
        input_layout_address_of_grocery = (TextInputLayout) view.findViewById(R.id.input_layout_address_of_grocery);
        input_layout_markup = (TextInputLayout) view.findViewById(R.id.input_layout_markup);

        onTextChangeListener(eNameOfGrocery, input_layout_name_of_grocery);
        onTextChangeListener(eAddressOfGrocery, input_layout_address_of_grocery);
        onTextChangeListener(eMarkupOfGrocery, input_layout_markup);

        view.findViewById(R.id.save).setOnClickListener(this);;
        view.findViewById(R.id.cancel).setOnClickListener(this);
        return view;
    }

    private void onTextChangeListener(final EditText e, final TextInputLayout textInputLayout){
        e.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0)
                    e.setError(null);
                textInputLayout.setErrorEnabled(false);
            }
        });
    }

    @Override
    public void onClick(View v) {
        name = eNameOfGrocery.getText().toString();
        address = eAddressOfGrocery.getText().toString();
        selection = DataBaseHelper.GROCERY_ID + "=?";

        switch (v.getId()) {
            case R.id.save:
                if (name.matches("")){
                    input_layout_name_of_grocery.setErrorEnabled(true);
                    input_layout_name_of_grocery.setError(getResources().getString(R.string.enter_name_of_grocery));
                    eNameOfGrocery.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }
                if(address.matches("")) {
                    input_layout_address_of_grocery.setErrorEnabled(true);
                    input_layout_address_of_grocery.setError(getResources().getString(R.string.enter_address_of_grocery));
                    eAddressOfGrocery.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }

                try {
                    markup = Double.parseDouble(eMarkupOfGrocery.getText().toString());
                }
                catch (NumberFormatException e) {
                    input_layout_markup.setErrorEnabled(true);
                    input_layout_markup.setError(getResources().getString(R.string.enter_numeric_value));
                    eMarkupOfGrocery.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }
                cv.clear();
                cv.put(DataBaseHelper.TYPE_OF_GROCERY, typeOfGrocery);
                cv.put(DataBaseHelper.NAME_OF_GROCERY, name);
                cv.put(DataBaseHelper.ADDRESS, address);
                cv.put(DataBaseHelper.MARKUP, markup);
                Uri singleUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, idOfGrocery);
                cr.update(singleUri, cv, selection, new String[]{String.valueOf(idOfGrocery)});
                dismiss();
                break;
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        TextKeyListener.clear(eNameOfGrocery.getText());
        TextKeyListener.clear(eAddressOfGrocery.getText());
        TextKeyListener.clear(eMarkupOfGrocery.getText());


    }
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        selection = DataBaseHelper.GROCERY_ID + "=?";
        projection = new String []{
                DataBaseHelper.GROCERY_ID + " AS _id",
                DataBaseHelper.TYPE_OF_GROCERY,
                DataBaseHelper.NAME_OF_GROCERY,
                DataBaseHelper.ADDRESS,
                DataBaseHelper.MARKUP
        };
        selectionArgs = new String[]{String.valueOf(idOfGrocery)};
        return new EditGroceryLoader(getContext(),projection, selection, selectionArgs);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor != null && cursor.moveToFirst()) {
            do {
                eNameOfGrocery.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GROCERY)));
                eAddressOfGrocery.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.ADDRESS)));
                eMarkupOfGrocery.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.MARKUP)));
                typeOfGrocery = (cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.TYPE_OF_GROCERY)));
            } while (cursor.moveToNext());
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        loader.deliverResult(null);
    }
}
