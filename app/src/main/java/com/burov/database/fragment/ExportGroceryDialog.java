package com.burov.database.fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.burov.database.R;
import com.burov.database.loader.LoaderOfAllGroceries;
import com.burov.database.model.DataBaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Vitaliy on 03-Feb-16.
 */
public class ExportGroceryDialog extends DialogFragment implements View.OnClickListener,
        android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor> {

    private String name;
    EditText eName;
    private final String LOG_TAG = "myLogs";
    private TextInputLayout layout_name_of_file;
    private JSONArray jsonArrayGroceries;
    private JSONObject jsonObjectGrocery;
    public static ExportGroceryDialog newExportGroceryDialog() {
        return new ExportGroceryDialog();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(getResources().getString(R.string.export_grocery));
        View view = inflater.inflate(R.layout.export_grocery, container, false);
        layout_name_of_file.findViewById(R.id.layout_name_of_file);
        eName = (EditText) view.findViewById(R.id.e_name_of_file);
        view.findViewById(R.id.b_save).setOnClickListener(this);
        view.findViewById(R.id.b_cancel).setOnClickListener(this);
        return view;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.b_save:
                name = eName.getText().toString();
                if (name.isEmpty()) {
                    layout_name_of_file.setErrorEnabled(true);
                    layout_name_of_file.setError(getResources().getString
                            (R.string.enter_name_of_file));
                    eName.setError(getResources().getString(R.string.field_is_empty));
                }
                dismiss();
                break;
            case R.id.b_cancel:
                dismiss();
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new LoaderOfAllGroceries(getContext());
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor!=null && cursor.moveToFirst()) {
            do {
                try {
                            jsonObjectGrocery.put(DataBaseHelper.NAME_OF_GROCERY,
                                    cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GROCERY)));
                    jsonObjectGrocery.put(DataBaseHelper.ADDRESS,
                            cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.ADDRESS)));
                    jsonObjectGrocery.put(DataBaseHelper.MARKUP,
                            cursor.getDouble(cursor.getColumnIndexOrThrow(DataBaseHelper.MARKUP)));
                    jsonArrayGroceries.put(jsonObjectGrocery);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }while (cursor.moveToNext());

        }

    }
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
