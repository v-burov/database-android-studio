package com.burov.database.fragment;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.TextKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.burov.database.R;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.provider.DataBaseContentProvider;

/**
 * Created by Vitaliy on 08-Dec-15.
 */
public class ManufacturerInsertDialog  extends DialogFragment implements View.OnClickListener {
    EditText eNameOfManufacturer;
    EditText eDescriptionOfManufacturer;
    final String LOG_TAG = "myLogs";
    ContentValues cv;
    ContentResolver cr;
    private String nameOfManufacturer = "";
    TextInputLayout input_layout_name_of_manufacturer, input_layout_description_of_manufacturer;
    ManufacturerInsertListener fragment;

    public interface ManufacturerInsertListener {
        void onFinishManufacturerInsert(String nameOfManufacturer);
    }

    public static ManufacturerInsertDialog newInstanceManufacturerInsert() {
        ManufacturerInsertDialog fragment = new ManufacturerInsertDialog();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cr = getActivity().getContentResolver();
        cv = new ContentValues();
        fragment = (ManufacturerInsertListener) getTargetFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().setTitle(getResources().getString(R.string.add_manufacturer));

        View view = inflater.inflate(R.layout.manufacturer_insert_dialog, container, false);

        eNameOfManufacturer = (EditText)view.findViewById(R.id.e_name_of_manufacturer);
        eDescriptionOfManufacturer = (EditText)view.findViewById(R.id.e_description_of_manufacturer);

        eNameOfManufacturer.setOnClickListener(this);
        eDescriptionOfManufacturer.setOnClickListener(this);

        input_layout_name_of_manufacturer = (TextInputLayout) view.findViewById(R.id.input_layout_name_of_manufacturer);
        input_layout_description_of_manufacturer = (TextInputLayout) view.findViewById(R.id.input_layout_description_of_the_manufacturer);

        onTextChangeListener(eNameOfManufacturer, input_layout_name_of_manufacturer);
        onTextChangeListener(eDescriptionOfManufacturer, input_layout_description_of_manufacturer);

        view.findViewById(R.id.saveManufacturer).setOnClickListener(this);;
        view.findViewById(R.id.cancelManufacturer).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        String descriptionOfManufacturer;
        switch (v.getId()) {
            case R.id.saveManufacturer:
                nameOfManufacturer = eNameOfManufacturer.getText().toString();
                descriptionOfManufacturer = eDescriptionOfManufacturer.getText().toString();
                if (nameOfManufacturer.matches("")){
                    input_layout_name_of_manufacturer.setErrorEnabled(true);
                    input_layout_name_of_manufacturer.setError(getResources().getString(R.string.enter_name_of_manufacturer));
                    eNameOfManufacturer.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }
                if(descriptionOfManufacturer.matches("")) {
                    input_layout_description_of_manufacturer.setErrorEnabled(true);
                    input_layout_description_of_manufacturer.setError(getResources().getString(R.string.enter_description_of_manufacturer));
                    eDescriptionOfManufacturer.setError(getResources().getString(R.string.field_is_empty));
                    return;
                }

                cv.put(DataBaseHelper.NAME_OF_MANUFACTURER, nameOfManufacturer);
                cv.put(DataBaseHelper.DESCRIPTION_OF_MANUFACTURER, descriptionOfManufacturer);
                try {
                    cr.insert(DataBaseContentProvider.MANUFACTURER_CONTENT_URI, cv);
                }
                catch (SQLiteConstraintException e) {
                    input_layout_name_of_manufacturer.setErrorEnabled(true);
                    input_layout_name_of_manufacturer.setError(getResources().getString
                            (R.string.manufacturer_is_already_exist));
                    return;
                }
                dismiss();
                break;
            case R.id.cancelManufacturer:
                dismiss();
                break;
        }
    }

    private void onTextChangeListener(final EditText e, final TextInputLayout textInputLayout) {
        e.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0)
                    e.setError(null);
                textInputLayout.setErrorEnabled(false);
            }
        });
    }
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        eNameOfManufacturer.setError(null);
        eDescriptionOfManufacturer.setError(null);
        if (getActivity()!=null) {
            fragment.onFinishManufacturerInsert(nameOfManufacturer);
        }
    }
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}
