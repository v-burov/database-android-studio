package com.burov.database.provider;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.burov.database.model.DataBaseHelper;

import java.util.ArrayList;

/**
 * Created by home on 19.08.2015.
 */
public class DataBaseContentProvider extends ContentProvider {

    final String LOG_TAG = "myLogs";
    private DataBaseHelper dataBaseHelper;
    SQLiteDatabase db;

    // authority
    private static final String AUTHORITY = "com.burov.database.provider.DataBaseContentProvider";

    // path
    private static final String GROCERY_PATH = DataBaseHelper.GROCERY;
    private static final String GOODS_GROCERY_PATH = DataBaseHelper.GOODS_GROCERY;
    private static final String SUPPLIER_PATH = DataBaseHelper.SUPPLIER;
    private static final String MANUFACTURER_PATH = DataBaseHelper.MANUFACTURER;
    private static final String GOODS_PATH = DataBaseHelper.GOODS;

    // Общий Uri
    //grocery
    public static final Uri GROCERY_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + GROCERY_PATH);
    //goodsGrocery
    public static final Uri GOODS_GROCERY_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + GOODS_GROCERY_PATH);

    //supplier
    public static final Uri SUPPLIER_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + SUPPLIER_PATH);

    //manufacturer
    public static final Uri MANUFACTURER_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + MANUFACTURER_PATH);

    //goods
    public static final Uri GOODS_CONTENT_URI =  Uri.parse("content://"
            + AUTHORITY + "/" + GOODS_PATH);

    // набор строк 
    // for grocery table
    private static final String GROCERY_CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + GROCERY_PATH;
    private static final String GROCERY_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
            + AUTHORITY + "." + GROCERY_PATH;
    // for goods table
    private static final String GOODS_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
            + AUTHORITY + "." + GOODS_GROCERY_PATH;
    // for supplier table
    private static final String SUPPLIER_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.."
            + AUTHORITY + "." + SUPPLIER_PATH;

    private static final String MANUFACTURER_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.."
            + AUTHORITY + "." + MANUFACTURER_PATH;

    private static final String GOODS_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.."
            + AUTHORITY + "." + GOODS_PATH;
    //// UriMatcher
    private static final int URI_ALL_GROCERIES = 1;
    private static final int URI_SINGLE_GROCERY_ID = 2;
    private static final int URI_SINGLE_GOODS_GROCERY_ID = 3;
    private static final int URI_SUPPLIERS = 4;
    private static final int URI_MANUFACTURERS = 5;
    private static final int URI_GOODS = 6;
    private static final int URI_SINGLE_GOODS_ID = 7;

    // описание и создание UriMatcher
    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, GROCERY_PATH, URI_ALL_GROCERIES);
        uriMatcher.addURI(AUTHORITY, GROCERY_PATH + "/#", URI_SINGLE_GROCERY_ID);
        uriMatcher.addURI(AUTHORITY, GOODS_GROCERY_PATH + "/#", URI_SINGLE_GOODS_GROCERY_ID);
        uriMatcher.addURI(AUTHORITY, SUPPLIER_PATH, URI_SUPPLIERS);
        uriMatcher.addURI(AUTHORITY, MANUFACTURER_PATH, URI_MANUFACTURERS);
        uriMatcher.addURI(AUTHORITY, GOODS_PATH, URI_GOODS);
        uriMatcher.addURI(AUTHORITY, GOODS_PATH + "/#", + URI_SINGLE_GOODS_ID);

    }

    @Override
    public boolean onCreate() {
        dataBaseHelper = new DataBaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        db = dataBaseHelper.getWritableDatabase();
        Cursor cursor = null;
        switch (uriMatcher.match(uri)) {

            case URI_ALL_GROCERIES:
                cursor = db.query(DataBaseHelper.GROCERY, projection, selection,
                        selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), GROCERY_CONTENT_URI);
                break;

            case URI_SUPPLIERS:
                cursor = db.query(DataBaseHelper.SUPPLIER, projection, selection,
                        selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), SUPPLIER_CONTENT_URI);
                break;

            case URI_MANUFACTURERS:
                cursor = db.query(DataBaseHelper.MANUFACTURER, projection, selection,
                        selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), MANUFACTURER_CONTENT_URI);
                break;

            case URI_GOODS:
                cursor = db.query(DataBaseHelper.GOODS, projection, selection,
                        selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), GOODS_CONTENT_URI);
                break;

            case URI_SINGLE_GOODS_ID:
                long goods_id = Long.parseLong(uri.getLastPathSegment());


                cursor = db.rawQuery("SELECT "

                        + "GG." + DataBaseHelper.GOODS_GROCERY_ID + ", "
                        + "GM." + DataBaseHelper.MANUFACTURING_DATE

                        + " FROM " + DataBaseHelper.GOODS_GROCERY

                        + " AS GG INNER JOIN " + DataBaseHelper.GOODS_SUPPLIER + " AS GS"
                        + " ON GS." + DataBaseHelper.GOODS_SUPPLIER_ID + " = " + "GG." + DataBaseHelper.GG_GOODS_SUPPLIER_ID

                        + " INNER JOIN " + DataBaseHelper.GOODS_MANUFACTURING + " AS GM"
                        + " ON GM." + DataBaseHelper.GOODS_MANUFACTURING_ID + " = " + "GS." + DataBaseHelper.GS_GOODS_MANUFACTURING_ID

                        + " INNER JOIN " + DataBaseHelper.GOODS + " AS A"
                        + " ON A." + DataBaseHelper.GOODS_ID + " = " + "GM." + DataBaseHelper.GM_GOODS_ID

                        + " WHERE " + DataBaseHelper.GOODS_ID + " = " + goods_id, null);

                cursor.setNotificationUri(getContext().getContentResolver(), GOODS_CONTENT_URI);
                break;


            case URI_SINGLE_GROCERY_ID:
                long idOfItem = Long.parseLong(uri.getLastPathSegment());
                cursor = db.rawQuery("SELECT "
                        + "GG." + DataBaseHelper.GOODS_GROCERY_ID + " AS _id, "
                        + "A." + DataBaseHelper.NAME_OF_GOODS + ", "
                        + "A." + DataBaseHelper.DESCRIPTION_OF_GOODS + ", "
                        + "A." + DataBaseHelper.PIECES_OR_GRAMS + ", "
                        + "A." + DataBaseHelper.APPLICATION_TIME + ", "
                        + "GG." + DataBaseHelper.WEIGHT_OF_GOODS + ", "
                        + "GG." + DataBaseHelper.NUMBER_OF_GOODS + ", "
                        + "GG." + DataBaseHelper.DELIVERY_DATE_TO_GROCERY + ", "
                        + "GS." + DataBaseHelper.BASE_PRICE + ", "
                        + "S." + DataBaseHelper.NAME_OF_SUPPLIER + ", "
                        + "S." + DataBaseHelper.DESCRIPTION_OF_SUPPLIER + ", "
                        + "M." + DataBaseHelper.NAME_OF_MANUFACTURER + ", "
                        + "M." + DataBaseHelper.DESCRIPTION_OF_MANUFACTURER + ", "
                        + "GM." + DataBaseHelper.MANUFACTURING_DATE + ", "
                        + "G." + DataBaseHelper.MARKUP + ", "
                        + "G." + DataBaseHelper.NAME_OF_GROCERY

                        + " FROM " + DataBaseHelper.GOODS_GROCERY

                        + " AS GG INNER JOIN " + DataBaseHelper.GOODS_SUPPLIER + " AS GS"
                        + " ON GS." + DataBaseHelper.GOODS_SUPPLIER_ID + " = " + "GG." + DataBaseHelper.GG_GOODS_SUPPLIER_ID
                        + " INNER JOIN " + DataBaseHelper.SUPPLIER + " AS S"
                        + " ON GS." + DataBaseHelper.GS_SUPPLIER_ID + " = " + "S." + DataBaseHelper.SUPPLIER_ID

                        + " INNER JOIN " + DataBaseHelper.GOODS_MANUFACTURING + " AS GM"
                        + " ON GM." + DataBaseHelper.GOODS_MANUFACTURING_ID + " = " + "GS." + DataBaseHelper.GS_GOODS_MANUFACTURING_ID
                        + " INNER JOIN " + DataBaseHelper.MANUFACTURER + " AS M"
                        + " ON M." + DataBaseHelper.MANUFACTURER_ID + " = " + "GM." + DataBaseHelper.GM_MANUFACTURER_ID

                        + " INNER JOIN " + DataBaseHelper.GOODS + " AS A"
                        + " ON A." + DataBaseHelper.GOODS_ID + " = " + "GM." + DataBaseHelper.GM_GOODS_ID

                        + " INNER JOIN " + DataBaseHelper.GROCERY + " AS G"
                        + " ON GG." + DataBaseHelper.GG_GROCERY_ID + " = " + "G." + DataBaseHelper.GROCERY_ID
                        + " WHERE " + DataBaseHelper.GG_GROCERY_ID + " = " + idOfItem, null);

                Uri singleUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, idOfItem);
                cursor.setNotificationUri(getContext().getContentResolver(), singleUri);
                break;

            case URI_SINGLE_GOODS_GROCERY_ID:
                long idOfGoods = Long.parseLong(uri.getLastPathSegment());
                cursor = db.rawQuery("SELECT "
                        + "G." + DataBaseHelper.GOODS_ID + " AS _id, "
                        + "G." + DataBaseHelper.NAME_OF_GOODS + ", "
                        + "G." + DataBaseHelper.PIECES_OR_GRAMS + ", "
                        + "G." + DataBaseHelper.DESCRIPTION_OF_GOODS + ", "
                        + "G." + DataBaseHelper.APPLICATION_TIME + ", "
                        + "GG." + DataBaseHelper.GOODS_GROCERY_ID + ", "
                        + "GG." + DataBaseHelper.GG_GROCERY_ID + ", "
                        + "GG." + DataBaseHelper.GG_GOODS_SUPPLIER_ID + ", "
                        + "GG." + DataBaseHelper.WEIGHT_OF_GOODS + ", "
                        + "GG." + DataBaseHelper.NUMBER_OF_GOODS + ", "
                        + "GG." + DataBaseHelper.DELIVERY_DATE_TO_GROCERY + ", "
                        + "GS." + DataBaseHelper.GOODS_SUPPLIER_ID + ", "
                        + "GS." + DataBaseHelper.GS_SUPPLIER_ID + ", "
                        + "GS." + DataBaseHelper.GS_GOODS_MANUFACTURING_ID + ", "
                        + "GS." + DataBaseHelper.BASE_PRICE + ", "
                        + "M." + DataBaseHelper.MANUFACTURER_ID + ", "
                        + "M." + DataBaseHelper.NAME_OF_MANUFACTURER + ", "
                        + "M." + DataBaseHelper.DESCRIPTION_OF_MANUFACTURER + ", "
                        + "S." + DataBaseHelper.SUPPLIER_ID + ", "
                        + "S." + DataBaseHelper.NAME_OF_SUPPLIER + ", "
                        + "S." + DataBaseHelper.DESCRIPTION_OF_SUPPLIER + ", "
                        + "GM." + DataBaseHelper.GOODS_MANUFACTURING_ID + ", "
                        + "GM." + DataBaseHelper.GM_MANUFACTURER_ID + ", "
                        + "GM." + DataBaseHelper.GM_GOODS_ID + ", "
                        + "GM." + DataBaseHelper.MANUFACTURING_DATE

                        + " FROM " + DataBaseHelper.GOODS_GROCERY

                        + " AS GG INNER JOIN " + DataBaseHelper.GOODS_SUPPLIER + " AS GS"
                        + " ON GG." + DataBaseHelper.GG_GOODS_SUPPLIER_ID + " = " + "GS." + DataBaseHelper.GOODS_SUPPLIER_ID
                        + " INNER JOIN " + DataBaseHelper.SUPPLIER + " AS S"
                        + " ON GS." + DataBaseHelper.GS_SUPPLIER_ID + " = " + "S." + DataBaseHelper.SUPPLIER_ID

                        + " INNER JOIN " + DataBaseHelper.GOODS_MANUFACTURING + " AS GM"
                        + " ON GM." + DataBaseHelper.GOODS_MANUFACTURING_ID + " = " + "GS." + DataBaseHelper.GS_GOODS_MANUFACTURING_ID
                        + " INNER JOIN " + DataBaseHelper.MANUFACTURER + " AS M"
                        + " ON M." + DataBaseHelper.MANUFACTURER_ID + " = " + "GM." + DataBaseHelper.GM_MANUFACTURER_ID

                        + " INNER JOIN " + DataBaseHelper.GOODS + " AS G"
                        + " ON G." + DataBaseHelper.GOODS_ID + " = " + "GM." + DataBaseHelper.GM_GOODS_ID

                        + " WHERE " + DataBaseHelper.GOODS_GROCERY_ID + " = " + idOfGoods, null);

                Uri singleGoods = ContentUris.withAppendedId(DataBaseContentProvider.GOODS_GROCERY_CONTENT_URI, idOfGoods);
                getContext().getContentResolver().notifyChange(singleGoods, null);
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case URI_SUPPLIERS:
                return SUPPLIER_CONTENT_TYPE;
            case URI_MANUFACTURERS:
                return MANUFACTURER_CONTENT_TYPE;
            case URI_ALL_GROCERIES:
                return GROCERY_CONTENT_TYPE;
            case URI_SINGLE_GROCERY_ID:
                return GROCERY_CONTENT_ITEM_TYPE;
            case URI_SINGLE_GOODS_GROCERY_ID:
                return GOODS_CONTENT_ITEM_TYPE;
            case URI_GOODS:
                return GOODS_CONTENT_TYPE;
        }
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        db = dataBaseHelper.getWritableDatabase();
        Uri resAllGroceries;
        Uri resSingleGrocery;
        long grocery_id = 0l;
        switch (uriMatcher.match(uri)) {
            case URI_GOODS:
                db.insertOrThrow(DataBaseHelper.GOODS, null, values);
                getContext().getContentResolver().notifyChange(GOODS_CONTENT_URI, null);
                return GOODS_CONTENT_URI;

            case URI_SUPPLIERS:
                db.insertOrThrow(DataBaseHelper.SUPPLIER, null, values);
                getContext().getContentResolver().notifyChange(SUPPLIER_CONTENT_URI, null);
                return SUPPLIER_CONTENT_URI;

            case URI_MANUFACTURERS:
                db.insertOrThrow(DataBaseHelper.MANUFACTURER, null, values);
                getContext().getContentResolver().notifyChange(MANUFACTURER_CONTENT_URI, null);
                return MANUFACTURER_CONTENT_URI;


            case URI_ALL_GROCERIES:
                grocery_id = db.insertOrThrow(DataBaseHelper.GROCERY, null, values);
                resAllGroceries = ContentUris.withAppendedId(GROCERY_CONTENT_URI, grocery_id);
                getContext().getContentResolver().notifyChange(resAllGroceries, null);
                return resAllGroceries;

            case URI_SINGLE_GROCERY_ID:
                grocery_id = Long.parseLong(uri.getLastPathSegment());
                ContentValues cv = new ContentValues();
                long goods_id = values.getAsLong(DataBaseHelper.GOODS_ID);
                double weightOfGoods = values.getAsDouble(DataBaseHelper.WEIGHT_OF_GOODS);
                int numOfGoods = values.getAsInteger(DataBaseHelper.NUMBER_OF_GOODS);
                double basePrice = values.getAsDouble(DataBaseHelper.BASE_PRICE);
                long supplier_id = values.getAsLong(DataBaseHelper.SUPPLIER_ID);
                long manufacturer_id = values.getAsLong(DataBaseHelper.MANUFACTURER_ID);
                String manufacturingDate = values.getAsString(DataBaseHelper.MANUFACTURING_DATE);
                String deliveryDateToGrocery = values.getAsString(DataBaseHelper.DELIVERY_DATE_TO_GROCERY);
                if (goods_id == -1) {
                    String nameOfGoods = values.getAsString(DataBaseHelper.NAME_OF_GOODS);
                    String descriptionOfGoods = values.getAsString(DataBaseHelper.DESCRIPTION_OF_GOODS);
                    int piecesOrGrams = values.getAsInteger(DataBaseHelper.PIECES_OR_GRAMS);
                    double applicationTime = values.getAsDouble(DataBaseHelper.APPLICATION_TIME);

                    cv.put(DataBaseHelper.NAME_OF_GOODS, nameOfGoods);
                    cv.put(DataBaseHelper.DESCRIPTION_OF_GOODS, descriptionOfGoods);
                    cv.put(DataBaseHelper.APPLICATION_TIME, applicationTime);
                    cv.put(DataBaseHelper.PIECES_OR_GRAMS, piecesOrGrams);
                    goods_id = db.insertOrThrow(DataBaseHelper.GOODS, null, cv);
                }
                cv.clear();
                cv.put(DataBaseHelper.GM_MANUFACTURER_ID, manufacturer_id);
                cv.put(DataBaseHelper.GM_GOODS_ID, goods_id);
                cv.put(DataBaseHelper.MANUFACTURING_DATE, manufacturingDate);
                long goodsManufacturing_id = db.insertOrThrow(DataBaseHelper.GOODS_MANUFACTURING, null, cv);

                cv.clear();
                cv.put(DataBaseHelper.GS_SUPPLIER_ID, supplier_id);
                cv.put(DataBaseHelper.GS_GOODS_MANUFACTURING_ID, goodsManufacturing_id);
                cv.put(DataBaseHelper.BASE_PRICE, basePrice);
                long goodsSupplier_id = db.insertOrThrow(DataBaseHelper.GOODS_SUPPLIER, null, cv);

                cv.clear();
                cv.put(DataBaseHelper.GG_GROCERY_ID, grocery_id);
                cv.put(DataBaseHelper.GG_GOODS_SUPPLIER_ID, goodsSupplier_id);
                cv.put(DataBaseHelper.WEIGHT_OF_GOODS, weightOfGoods);
                cv.put(DataBaseHelper.NUMBER_OF_GOODS, numOfGoods);
                cv.put(DataBaseHelper.DELIVERY_DATE_TO_GROCERY, deliveryDateToGrocery);
                db.insertOrThrow(DataBaseHelper.GOODS_GROCERY, null, cv);

                resSingleGrocery = ContentUris.withAppendedId(GROCERY_CONTENT_URI, grocery_id);
                getContext().getContentResolver().notifyChange(resSingleGrocery, null);
                return resSingleGrocery;
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        db = dataBaseHelper.getWritableDatabase();
        int count = 0;
        Cursor cursor;

        switch (uriMatcher.match(uri)) {
            case URI_SINGLE_GROCERY_ID:
                long grocery_id = Long.parseLong(uri.getLastPathSegment());
                long goodsManufacturing_id = 0l;
                long goodsGrocery_id = 0l;
                long goodsSupplier_id = 0l;
                ArrayList<Long> listOfGoodsGroceries = new ArrayList<Long>();
                ArrayList<Long> listOfGoodsSuppliers = new ArrayList<Long>();
                ArrayList<Long> listOfManufacturers = new ArrayList<Long>();

                cursor = db.rawQuery("SELECT "
                        + "GG." + DataBaseHelper.GOODS_GROCERY_ID + ", "
                        + "GS." + DataBaseHelper.GOODS_SUPPLIER_ID + ", "
                        + "GM." + DataBaseHelper.GOODS_MANUFACTURING_ID

                        + " FROM " + DataBaseHelper.GOODS_GROCERY

                        + " AS GG INNER JOIN " + DataBaseHelper.GOODS_SUPPLIER + " AS GS"
                        + " ON GG." + DataBaseHelper.GG_GOODS_SUPPLIER_ID + " = " + "GS." + DataBaseHelper.GOODS_SUPPLIER_ID

                        + " INNER JOIN " + DataBaseHelper.GOODS_MANUFACTURING + " AS GM"
                        + " ON GM." + DataBaseHelper.GOODS_MANUFACTURING_ID + " = " + "GS." + DataBaseHelper.GS_GOODS_MANUFACTURING_ID

                        + " WHERE " + DataBaseHelper.GG_GROCERY_ID + " = " + grocery_id, null);

                if (cursor!=null && cursor.moveToFirst()) {
                    do {
                        goodsGrocery_id = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_GROCERY_ID));
                        goodsSupplier_id = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_SUPPLIER_ID));
                        goodsManufacturing_id = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_MANUFACTURING_ID));
                        listOfGoodsGroceries.add(goodsGrocery_id);
                        listOfGoodsSuppliers.add(goodsSupplier_id);
                        listOfManufacturers.add(goodsManufacturing_id);
                    }while (cursor.moveToNext());
                }
                for (int i = 0; i < listOfGoodsGroceries.size();i++) {
                    db.delete(DataBaseHelper.GOODS_GROCERY, DataBaseHelper.GOODS_GROCERY_ID+"=?",
                            new String[]{String.valueOf(listOfGoodsGroceries.get(i))});
                }
                for (int i = 0; i < listOfGoodsSuppliers.size();i++) {
                    db.delete(DataBaseHelper.GOODS_SUPPLIER, DataBaseHelper.GOODS_SUPPLIER_ID+"=?",
                            new String[]{String.valueOf(listOfGoodsSuppliers.get(i))});
                }
                for (int i = 0; i < listOfManufacturers.size();i++) {
                    db.delete(DataBaseHelper.GOODS_MANUFACTURING, DataBaseHelper.GOODS_MANUFACTURING_ID+"=?",
                            new String[]{String.valueOf(listOfManufacturers.get(i))});
                }

                Uri resSingleGrocery = ContentUris.withAppendedId(GROCERY_CONTENT_URI, grocery_id);
                getContext().getContentResolver().notifyChange(resSingleGrocery, null);
                count = db.delete(DataBaseHelper.GROCERY, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                return count;

            case URI_SINGLE_GOODS_GROCERY_ID:
                long goods_id = Long.parseLong(uri.getLastPathSegment());
                goodsManufacturing_id = 0l;
                goodsGrocery_id = 0l;
                goodsSupplier_id = 0l;

                cursor = db.rawQuery("SELECT "
                        + "GG." + DataBaseHelper.GOODS_GROCERY_ID + ", "
                        + "GS." + DataBaseHelper.GOODS_SUPPLIER_ID + ", "
                        + "GM." + DataBaseHelper.GOODS_MANUFACTURING_ID

                        + " FROM " + DataBaseHelper.GOODS_GROCERY

                        + " AS GG INNER JOIN " + DataBaseHelper.GOODS_SUPPLIER + " AS GS"
                        + " ON GG." + DataBaseHelper.GG_GOODS_SUPPLIER_ID + " = " + "GS." + DataBaseHelper.GOODS_SUPPLIER_ID

                        + " INNER JOIN " + DataBaseHelper.GOODS_MANUFACTURING + " AS GM"
                        + " ON GM." + DataBaseHelper.GOODS_MANUFACTURING_ID + " = " + "GS." + DataBaseHelper.GS_GOODS_MANUFACTURING_ID

                        + " WHERE " + DataBaseHelper.GOODS_GROCERY_ID+ " = " + goods_id, null);

                if (cursor!=null && cursor.moveToFirst()) {
                    goodsGrocery_id = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_GROCERY_ID));
                    goodsSupplier_id = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_SUPPLIER_ID));
                    goodsManufacturing_id = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_MANUFACTURING_ID));
                }

                db.delete(DataBaseHelper.GOODS_GROCERY, DataBaseHelper.GOODS_GROCERY_ID+"=?", new String[]{String.valueOf(goodsGrocery_id)});
                db.delete(DataBaseHelper.GOODS_SUPPLIER, DataBaseHelper.GOODS_SUPPLIER_ID+"=?", new String[]{String.valueOf(goodsSupplier_id)});
                db.delete(DataBaseHelper.GOODS_MANUFACTURING, DataBaseHelper.GOODS_MANUFACTURING_ID+"=?", new String[]{String.valueOf(goodsManufacturing_id)});

                Uri resSingleGoods = ContentUris.withAppendedId(GOODS_GROCERY_CONTENT_URI, goods_id);
                getContext().getContentResolver().notifyChange(resSingleGoods, null);
                }

                getContext().getContentResolver().notifyChange(GROCERY_CONTENT_URI, null);
                return count;
    }
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int countUpdate = 0;
        Uri resSingleGoods;
        db = dataBaseHelper.getWritableDatabase();
        switch (uriMatcher.match(uri)) {
            case URI_SINGLE_GROCERY_ID:
                countUpdate = db.update(DataBaseHelper.GROCERY, values, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                return countUpdate;
            case URI_SINGLE_GOODS_GROCERY_ID:
                //data for updating the goods
                long goodsGrocery_id = Long.parseLong(uri.getLastPathSegment());
                long gmManufacturer_id = values.getAsLong(DataBaseHelper.GM_MANUFACTURER_ID);
                long gmGoods_id = values.getAsLong(DataBaseHelper.GM_GOODS_ID);
                String manufacturingDate = values.getAsString(DataBaseHelper.MANUFACTURING_DATE);
                long goodsManufacturing_id = values.getAsLong(DataBaseHelper.GOODS_MANUFACTURING_ID);

                long gsSupplier_id = values.getAsLong(DataBaseHelper.GS_SUPPLIER_ID);
                long gsGoodsManufacturing_id = values.getAsLong(DataBaseHelper.GS_GOODS_MANUFACTURING_ID);
                double basePrice = values.getAsDouble(DataBaseHelper.BASE_PRICE);
                long goodsSupplier_id = values.getAsLong(DataBaseHelper.GOODS_SUPPLIER_ID);

                long ggGrocery_id = values.getAsLong(DataBaseHelper.GG_GROCERY_ID);
                long ggGoodsSupplier_id = values.getAsLong(DataBaseHelper.GG_GOODS_SUPPLIER_ID);
                double weightOfGoods = values.getAsDouble(DataBaseHelper.WEIGHT_OF_GOODS);
                int numOfGoods = values.getAsInteger(DataBaseHelper.NUMBER_OF_GOODS);
                String deliveryDateToGrocery = values.getAsString(DataBaseHelper.DELIVERY_DATE_TO_GROCERY);

                ContentValues cv = new ContentValues();

                cv.clear();
                cv.put(DataBaseHelper.GM_MANUFACTURER_ID, gmManufacturer_id);
                cv.put(DataBaseHelper.GM_GOODS_ID, gmGoods_id);
                cv.put(DataBaseHelper.MANUFACTURING_DATE, manufacturingDate);
                db.update(DataBaseHelper.GOODS_MANUFACTURING, cv, DataBaseHelper.GOODS_MANUFACTURING_ID + "=?",
                        new String[]{String.valueOf(goodsManufacturing_id)});

                cv.clear();
                cv.put(DataBaseHelper.GS_SUPPLIER_ID, gsSupplier_id);
                cv.put(DataBaseHelper.GS_GOODS_MANUFACTURING_ID, gsGoodsManufacturing_id);
                cv.put(DataBaseHelper.BASE_PRICE, basePrice);
                db.update(DataBaseHelper.GOODS_SUPPLIER, cv, DataBaseHelper.GOODS_SUPPLIER_ID + "=?",
                        new String[]{String.valueOf(goodsSupplier_id)});

                cv.clear();
                cv.put(DataBaseHelper.GG_GROCERY_ID, ggGrocery_id);
                cv.put(DataBaseHelper.GG_GOODS_SUPPLIER_ID, ggGoodsSupplier_id);
                cv.put(DataBaseHelper.WEIGHT_OF_GOODS, weightOfGoods);
                cv.put(DataBaseHelper.NUMBER_OF_GOODS, numOfGoods);
                cv.put(DataBaseHelper.DELIVERY_DATE_TO_GROCERY, deliveryDateToGrocery);
                db.update(DataBaseHelper.GOODS_GROCERY, cv, DataBaseHelper.GOODS_GROCERY_ID + "=?",
                        new String[]{String.valueOf(goodsGrocery_id)});

                resSingleGoods = ContentUris.withAppendedId(GOODS_GROCERY_CONTENT_URI, goodsGrocery_id);
                getContext().getContentResolver().notifyChange(resSingleGoods, null);

                Uri resSingleGrocery = ContentUris.withAppendedId(GROCERY_CONTENT_URI, ggGrocery_id);
                getContext().getContentResolver().notifyChange(resSingleGrocery, null);

               return countUpdate;
            default:
                return 0;
        }
    }
}

