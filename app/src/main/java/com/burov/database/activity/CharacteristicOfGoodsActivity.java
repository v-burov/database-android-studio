package com.burov.database.activity;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.burov.database.provider.DataBaseContentProvider;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.R;

/**
 * Created by home on 28.09.2015.
 */
public class CharacteristicOfGoodsActivity extends AppCompatActivity {

    private long idOfGoods = 0L;
    private String nameOfGoods;
    Cursor cursor = null;
    private final String LOG_TAG = "myLogs";

    TextView descriptionOfGoods = null;
    private String description = null;

    TextView applicationTimeOfGoods = null;
    private String applicationTime = null;

    TextView manufacturerOfGoods = null;
    private String manufacturer = null;

    TextView supplierOfGoods = null;
    private String supplier = null;

    TextView manufacturingDateOfGoods = null;
    private String manufacturingDate = null;

    TextView deliveryDateToGroceryOfGoods = null;
    private String deliveryDateToGrocery = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.characteristic_of_goods_activity);

        Toolbar toolbar = (Toolbar) this.findViewById(R.id.characteristicOfGoodsToolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null && getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        descriptionOfGoods = (TextView) findViewById(R.id.description_of_goods);
        manufacturerOfGoods = (TextView) findViewById(R.id.manufacturer_of_goods);
        supplierOfGoods = (TextView) findViewById(R.id.supplier_of_goods);
        manufacturingDateOfGoods = (TextView) findViewById(R.id.manufacturing_date);
        deliveryDateToGroceryOfGoods = (TextView) findViewById(R.id.delivery_date_to_grocery);
        applicationTimeOfGoods = (TextView) findViewById(R.id.application_time);

        ContentResolver cr = getApplicationContext().getContentResolver();
        idOfGoods = getIntent().getLongExtra("Id of goods", 0l);
        nameOfGoods = getIntent().getStringExtra("Name of the goods");
        //toolbar.setTitle(nameOfGoods);

        Uri singleUri = ContentUris.withAppendedId(DataBaseContentProvider.GOODS_GROCERY_CONTENT_URI, idOfGoods);
        cursor = cr.query(singleUri, null, null, null, null);
        cursor.setNotificationUri(getContentResolver(),
                singleUri);
        if( cursor != null && cursor.moveToFirst()) {
            String nameOfGoods2 = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GOODS));
            //Log.d(LOG_TAG, nameOfGoods2);
            toolbar.setTitle(nameOfGoods2);

            description = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.DESCRIPTION_OF_GOODS));
            manufacturer = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_MANUFACTURER));
            supplier = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_SUPPLIER));
            manufacturingDate = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.MANUFACTURING_DATE));
            deliveryDateToGrocery = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.DELIVERY_DATE_TO_GROCERY));
            applicationTime = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.APPLICATION_TIME));
        }
        cursor.close();

        descriptionOfGoods.setText(description);
        manufacturerOfGoods.setText(manufacturer);
        supplierOfGoods.setText(supplier);
        manufacturingDateOfGoods.setText(manufacturingDate);
        deliveryDateToGroceryOfGoods.setText(deliveryDateToGrocery);
        applicationTimeOfGoods.setText(applicationTime);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_navigator, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
