package com.burov.database.activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.burov.database.Service.ExportGoodsService;
import com.burov.database.Service.LoadGoodsFromServer;
import com.burov.database.adapter.GoodsCursorAdapter;
import com.burov.database.fragment.GoodsChoiceDialog;
import com.burov.database.fragment.GoodsEditFragment;
import com.burov.database.loader.GoodsLoader;
import com.burov.database.R;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.provider.DataBaseContentProvider;
import android.support.v4.content.Loader;
import net.rdrei.android.dirchooser.DirectoryChooserConfig;
import net.rdrei.android.dirchooser.DirectoryChooserFragment;
import android.support.v4.widget.SwipeRefreshLayout;

/**
 * Created by home on 08.09.2015.
 */
public class ListOfGoodsActivity extends AppCompatActivity implements
        android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor>,
        DirectoryChooserFragment.OnFragmentInteractionListener,
        SwipeRefreshLayout.OnRefreshListener {
    ListView list;
    GoodsCursorAdapter adapter;
    private static final int LOADER_ID = 1;
    private long idOfGrocery = 0L;
    private final String LOG_TAG = "myLogs";
    FloatingActionButton fab = null;
    GoodsEditFragment goodsEditFragment;
    GoodsChoiceDialog goodsChoiceDialog;
    FragmentTransaction fTrans;
    private DirectoryChooserFragment mDialog;
    private static final String SAVING_PATH = "savingPath";
    private static final String GROCERY_ID = "groceryId";
    private long idOfGoods;

    @Override
    public void onRefresh() {
        // start intent service here
        Intent intentService = new Intent(this, LoadGoodsFromServer.class);
        intentService.putExtra(GROCERY_ID, idOfGrocery);
                startService(intentService);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    Cursor cursor;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_of_goods_activity);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefreshGoods);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        Toolbar toolbar = (Toolbar) this.findViewById(R.id.goodsToolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null && getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   onBackPressed();
                }
            });
        }
        list = (ListView) findViewById(R.id.list_of_goods);
        idOfGrocery = getIntent().getLongExtra(GROCERY_ID, 0l);

        fab = (FloatingActionButton) findViewById(R.id.goods_fab);
        goodsChoiceDialog = new GoodsChoiceDialog().newGoodsChoiceDialog(idOfGrocery);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goodsChoiceDialog.show(getSupportFragmentManager(), "dialog");
            }
        });
        getSupportLoaderManager().initLoader(LOADER_ID, null, this);
        adapter = new GoodsCursorAdapter(this,null,0);
        list.setAdapter(adapter);
        registerForContextMenu(list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent i = new Intent(view.getContext(), CharacteristicOfGoodsActivity.class);
                idOfGoods = adapter.getItemId(position);
                i.putExtra("Id of goods", idOfGoods);
                cursor = adapter.getCursor();
                String nameOfGoods = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GOODS));
                i.putExtra("Name of the goods", nameOfGoods);
                startActivity(i);
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new GoodsLoader(this, idOfGrocery);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        adapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
    //Context menu
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_of_the_listview, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        ContentResolver cr = getContentResolver();
        idOfGoods = info.id;
        switch(item.getItemId()) {
            case R.id.edit:
                fab.hide();
                goodsEditFragment = new GoodsEditFragment().newInstanceEditGoods(idOfGoods, idOfGrocery);
                fTrans = getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.fragCont, goodsEditFragment);
                fTrans.commit();
                return true;
            case R.id.delete:
                Uri singleGroceryUri = ContentUris.withAppendedId(DataBaseContentProvider.GOODS_GROCERY_CONTENT_URI, idOfGoods);
                cr.delete(singleGroceryUri, null, null);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_navigator, menu);
        MenuItem importGrocery = menu.findItem(R.id.import_grocery);
        importGrocery.setVisible(false);
        MenuItem exportGrocery = menu.findItem(R.id.export_grocery);
        exportGrocery.setVisible(false);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                break;
            case android.R.id.home:
                break;
            case R.id.export_goods:
                String m_sdcardDirectory = getFilesDir().toString();
                final DirectoryChooserConfig config = DirectoryChooserConfig.builder()
                        .newDirectoryName("")
                        .allowNewDirectoryNameModification(true)
                        .allowReadOnlyDirectory(true)
                        .initialDirectory(m_sdcardDirectory)
                        .build();
                mDialog = DirectoryChooserFragment.newInstance(config);
                mDialog.show(getFragmentManager(), null);
            break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onSelectDirectory(@NonNull final String path) {
        Intent intent = new Intent(this, ExportGoodsService.class);
        intent.putExtra(SAVING_PATH, path);
        intent.putExtra(GROCERY_ID, idOfGrocery);
        startService(intent);
        mDialog.dismiss();
    }

    @Override
    public void onCancelChooser() {
        mDialog.dismiss();
    }
}
