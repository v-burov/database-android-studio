package com.burov.database.activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.burov.database.R;
import com.burov.database.Service.ExportGroceryService;
import com.burov.database.adapter.SampleFragmentPagerAdapter;
import com.burov.database.loader.LoaderOfAllGroceries;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


import net.rdrei.android.dirchooser.DirectoryChooserConfig;
import net.rdrei.android.dirchooser.DirectoryChooserFragment;

public class MainActivity extends AppCompatActivity implements DirectoryChooserFragment.OnFragmentInteractionListener {

	private final String LOG_TAG = "myLogs";
	private DirectoryChooserFragment mDialog;
	private static final String SAVING_PATH = "savingPath";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Get the ViewPager and set it's PagerAdapter so that it can display items
		final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

		viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager(),
				MainActivity.this));


		// Give the TabLayout the ViewPager
		final TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
		tabLayout.setupWithViewPager(viewPager);

		Toolbar toolbar = (Toolbar) this.findViewById(R.id.toolbar);
		//toolbar.setTitleTextColor(Color.WHITE);
		setSupportActionBar(toolbar);



	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_home_navigator, menu);
		MenuItem importGoods = menu.findItem(R.id.import_goods);
		importGoods.setVisible(false);
		MenuItem exportGoods = menu.findItem(R.id.export_goods);
		exportGoods.setVisible(false);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case R.id.export_grocery:
				String m_sdcardDirectory = getFilesDir().toString();
				final DirectoryChooserConfig config = DirectoryChooserConfig.builder()
						.newDirectoryName("")
						.allowNewDirectoryNameModification(true)
						.allowReadOnlyDirectory(true)
						.initialDirectory(m_sdcardDirectory)
						.build();
				mDialog = DirectoryChooserFragment.newInstance(config);
				mDialog.show(getFragmentManager(), null);
				break;
			case R.id.import_grocery:
				break;
			case R.id.action_settings:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onSelectDirectory(@NonNull final String path) {
		Intent intentIntentService = new Intent(this, ExportGroceryService.class);
		startService(intentIntentService.putExtra(SAVING_PATH, path));
		mDialog.dismiss();
	}

	@Override
	public void onCancelChooser() {
		mDialog.dismiss();
	}
}