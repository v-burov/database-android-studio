package com.burov.database.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.burov.database.fragment.PageFragment;
import com.burov.database.R;

/**
 * Created by home on 30.07.2015.
 */
public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {

    private TypeOfGroceryTab[] typeOfGroceryTab = {TypeOfGroceryTab.SupermarketyTab, TypeOfGroceryTab.GastronomyTab, TypeOfGroceryTab.KioskiTab};

    private Context context;

    public SampleFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return typeOfGroceryTab.length;
    }

    @Override
    public Fragment getItem(int position) {

        return PageFragment.newInstance(typeOfGroceryTab[position]);
    }
    //
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (typeOfGroceryTab[position]) {

            case SupermarketyTab:
                return context.getString(R.string.supermarkety);

            case GastronomyTab:
                return context.getString(R.string.gastronomy);

            case KioskiTab:
                return context.getString(R.string.kioski);
            default: return "";
        }
    }
    public enum TypeOfGroceryTab {
        SupermarketyTab,
        GastronomyTab,
        KioskiTab

    }
}