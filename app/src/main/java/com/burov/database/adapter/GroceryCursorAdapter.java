package com.burov.database.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.burov.database.model.DataBaseHelper;
import com.burov.database.R;

/**
 * Created by home on 18.08.2015.
 */
public class GroceryCursorAdapter extends CursorAdapter {

    Context context;
    //final String LOG_TAG = "myLogs";

    public GroceryCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);
        this.context = context;
        //cursorInflater = (LayoutInflater) context.getSystemService(
        //Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        //ContentResolver resolver = Context.getContentResolver();
        // ContentResolver cr = context.getContentResolver().query(Uri uri, projection, String selection, String[] selectionArgs, String sortOrder);
        return LayoutInflater.from(context).inflate(R.layout.listview_grocery, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        int typeOfGrocery = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.TYPE_OF_GROCERY));
        //Log.d(LOG_TAG, typeOfGrocery + "type of grocery");
        String nameOfGrocery = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GROCERY));
        String addressOfGrocery = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.ADDRESS));


        TextView textViewNameOfGrocery = (TextView) view.findViewById(R.id.tv_name_of_the_grocery);
        if (textViewNameOfGrocery != null) {
            textViewNameOfGrocery.setText(nameOfGrocery);
        }

        TextView textViewAddressOfGrocery = (TextView) view.findViewById(R.id.address_of_the_grocery);
        if (textViewAddressOfGrocery != null) {
            textViewAddressOfGrocery.setText(addressOfGrocery);
        }
    }

}



