package com.burov.database.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.burov.database.model.DataBaseHelper;
import com.burov.database.R;

/**
 * Created by home on 30.08.2015.
 */
public class GoodsCursorAdapter extends CursorAdapter {
    Context context;
    private final String LOG_TAG = "myLogs";

    public GoodsCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);
        this.context = context;
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        //ContentResolver resolver = Context.getContentResolver();
        // ContentResolver cr = context.getContentResolver().query(Uri uri, projection, String selection, String[] selectionArgs, String sortOrder);
        return LayoutInflater.from(context).inflate(R.layout.list_of_goods, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String nameOfGoods = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GOODS));
        double basePrice = cursor.getDouble(cursor.getColumnIndexOrThrow(DataBaseHelper.BASE_PRICE));
        double weightOfGoods = cursor.getDouble(cursor.getColumnIndexOrThrow(DataBaseHelper.WEIGHT_OF_GOODS));
        int numOfGoods = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.NUMBER_OF_GOODS));
        double markup = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.MARKUP));

        TextView textViewNameOfGoods = (TextView) view.findViewById(R.id.name_of_goods);
        TextView textViewPrice = (TextView) view.findViewById(R.id.price);
        TextView textViewWeightOrNum = (TextView) view.findViewById(R.id.text_weight_or_pieces);
        TextView textViewValueWeightOrPieces = (TextView) view.findViewById(R.id.value_weight_or_pieces);

        if (weightOfGoods != 0) {
            textViewNameOfGoods.setText(nameOfGoods);
            textViewValueWeightOrPieces.setText(weightOfGoods + "");
            textViewPrice.setText(basePrice+markup+" ");
            textViewWeightOrNum.setText("кг/литр");
        }
        else if (numOfGoods != 0) {
            textViewNameOfGoods.setText(nameOfGoods);
            textViewValueWeightOrPieces.setText(numOfGoods + "");
            textViewPrice.setText(basePrice+markup+" ");
            textViewWeightOrNum.setText("штук(у)");

        }

    }

}
