package com.burov.database.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "grocery.db";
	public static final int DATABASE_VERSION = 1;
	//final String LOG_TAG = "myLogs";

	//grocery_table
	public static final String GROCERY = "groceryTable";
	public static final String GROCERY_ID = "grocery_id";
	public static final String TYPE_OF_GROCERY = "typeOfGrocery";
	public static final String NAME_OF_GROCERY = "nameOfGrocery";
	public static final String ADDRESS = "address";
	public static final String MARKUP = "markup";
	
	//goods_table
	public static final String GOODS = "goodsTable";
	public static final String GOODS_ID = "goods_id";
	public static final String NAME_OF_GOODS = "nameOfGoods";
	public static final String DESCRIPTION_OF_GOODS = "descriptionOfGoods";
	public static final String APPLICATION_TIME = "applicationTime";
	public static final String PIECES_OR_GRAMS = "PiecesOrGrams";

	//goods_grocery_table
	public static final String GOODS_GROCERY = "goodsGroceryTable";
	public static final String GOODS_GROCERY_ID = "goodsGrocery_id";
	public static final String GG_GROCERY_ID = "goodsGroceryGrocery_id";
	public static final String GG_GOODS_SUPPLIER_ID = "goodsGroceryGoodsSupplier_id";
	public static final String WEIGHT_OF_GOODS = "weightOfGoods";
	public static final String NUMBER_OF_GOODS = "numberOfGoods";
	public static final String DELIVERY_DATE_TO_GROCERY = "deliveryDateToGrocery";

	//supplier_table
	public static final String SUPPLIER = "supplierTable";
	public static final String SUPPLIER_ID = "supplier_id";
	public static final String NAME_OF_SUPPLIER = "nameOfSupplier";
	public static final String DESCRIPTION_OF_SUPPLIER = "descriptionOfSupplier";

	//goods_supplier_table
	public static final String GOODS_SUPPLIER = "goodsSupplierTable";
	public static final String GOODS_SUPPLIER_ID = "goodsSupplier_id";
	public static final String GS_GOODS_MANUFACTURING_ID = "gsGoodsManufacturing_id";
	public static final String GS_SUPPLIER_ID = "goodsSupplierSupplier_id";
	public static final String BASE_PRICE = "basePrice";

	//manufacturer table
	public static final String MANUFACTURER = "manufacturerTable";
	public static final String MANUFACTURER_ID = "manufacturer_id";
	public static final String NAME_OF_MANUFACTURER = "nameOfManufacturer";
	public static final String DESCRIPTION_OF_MANUFACTURER = "descriptionOfManufacturer";

	//goods_manufacturing_date_table
	public static final String GOODS_MANUFACTURING = "goodsManufacturing";
	public static final String GOODS_MANUFACTURING_ID = "goodsManufacturing_id";
	public static final String GM_MANUFACTURER_ID = "goodsManufacturingManufacturer_id";
	public static final String MANUFACTURING_DATE = "manufacturingDate";
	public static final String GM_GOODS_ID = "goodsManufacturingGoods_id";
	// create grocery_table
	private static final String SQL_CREATE_ENTRIES_GROCERY = "CREATE TABLE "
            + GROCERY
            + " (" + GROCERY_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TYPE_OF_GROCERY + " INTEGER, "+ NAME_OF_GROCERY +" TEXT unique, "+ADDRESS+" TEXT, "
			+ MARKUP+ " REAL);";
	
	//create goods_table
	private static final String SQL_CREATE_ENTRIES_GOODS = "CREATE TABLE "
            + GOODS
            + " (" + GOODS_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NAME_OF_GOODS +" TEXT unique, "+DESCRIPTION_OF_GOODS+" TEXT, "
			+ APPLICATION_TIME+" REAL, "+PIECES_OR_GRAMS+" INTEGER);";

	//create join goods_grocery_table
	private static final String SQL_CREATE_ENTRIES_GOODS_GROCERY = "CREATE TABLE " 
			+ GOODS_GROCERY
			+ " (" + GOODS_GROCERY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ GG_GROCERY_ID + " INTEGER, "
			+ GG_GOODS_SUPPLIER_ID + " INTEGER, "
			+ DELIVERY_DATE_TO_GROCERY + " TEXT, "
		    + WEIGHT_OF_GOODS+" REAL, "+NUMBER_OF_GOODS+" INTEGER, "
			+ " FOREIGN KEY("+ GG_GROCERY_ID +") REFERENCES " + GROCERY + "("+GROCERY_ID+"),"
			+ " FOREIGN KEY("+ GG_GOODS_SUPPLIER_ID +") REFERENCES "
			+ GOODS_SUPPLIER + "("+GOODS_SUPPLIER_ID+"));";

	//create supplier_table
	private static final String SQL_CREATE_ENTRIES_SUPPLIER = "CREATE TABLE "
            + SUPPLIER
            + " (" + SUPPLIER_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NAME_OF_SUPPLIER +" TEXT unique, "+DESCRIPTION_OF_SUPPLIER+" TEXT);";
	
	// join goods_supplier_table
	private static final String SQL_CREATE_ENTRIES_GOODS_SUPPLIER =	"CREATE TABLE "
			+ GOODS_SUPPLIER
			+ " ("+ GOODS_SUPPLIER_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ GS_SUPPLIER_ID + " INTEGER, " + GS_GOODS_MANUFACTURING_ID + " INTEGER, "
			+ BASE_PRICE +" REAL,"
			+" FOREIGN KEY ("+ GS_SUPPLIER_ID +") REFERENCES "+ SUPPLIER + "("+SUPPLIER_ID+"),"
			+" FOREIGN KEY ("+ GS_GOODS_MANUFACTURING_ID +") REFERENCES "+ GOODS_MANUFACTURING
			+ "("+GOODS_MANUFACTURING_ID +"));";
	
	//manufacturer table
	private static final String SQL_CREATE_ENTRIES_MANUFACTURER = "CREATE TABLE "
			+ MANUFACTURER
			+ " (" + MANUFACTURER_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ NAME_OF_MANUFACTURER +" TEXT unique, "+DESCRIPTION_OF_MANUFACTURER+" TEXT);";

	//create join goods_manufacturing_table
	private static final String SQL_CREATE_ENTRIES_GOODS_MANUFACTURING = "CREATE TABLE "
			+ GOODS_MANUFACTURING
			+ " (" + GOODS_MANUFACTURING_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ GM_MANUFACTURER_ID +" INTEGER, "
			+ GM_GOODS_ID +" INTEGER, "
			+ MANUFACTURING_DATE +" INTEGER,"
			+ " FOREIGN KEY("+ GM_MANUFACTURER_ID +") REFERENCES "
			+ MANUFACTURER + "("+MANUFACTURER_ID+"),"
			+ " FOREIGN KEY("+ GM_GOODS_ID +") REFERENCES "
			+ GOODS + "("+GOODS_ID+"));";

	public DataBaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}   
	
	@Override
    public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_ENTRIES_GROCERY);
		db.execSQL(SQL_CREATE_ENTRIES_MANUFACTURER);
		db.execSQL(SQL_CREATE_ENTRIES_GOODS);
		db.execSQL(SQL_CREATE_ENTRIES_SUPPLIER);
		db.execSQL(SQL_CREATE_ENTRIES_GOODS_MANUFACTURING);
		db.execSQL(SQL_CREATE_ENTRIES_GOODS_GROCERY);
		db.execSQL(SQL_CREATE_ENTRIES_GOODS_SUPPLIER);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    	db.execSQL("DROP TABLE IF EXISTS " + GROCERY);
    	db.execSQL("DROP TABLE IF EXISTS " + GOODS);
    	db.execSQL("DROP TABLE IF EXISTS " + GOODS_GROCERY);
    	db.execSQL("DROP TABLE IF EXISTS " + SUPPLIER);
    	db.execSQL("DROP TABLE IF EXISTS " + GOODS_SUPPLIER);
    	db.execSQL("DROP TABLE IF EXISTS " + MANUFACTURER);
    	db.execSQL("DROP TABLE IF EXISTS " + GOODS_MANUFACTURING);
    	onCreate(db);
    }
}