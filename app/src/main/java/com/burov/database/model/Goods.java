package com.burov.database.model;

import com.google.gson.annotations.SerializedName;

public class Goods {
	@SerializedName("nameOfGoods")
	private String nameOfGoods;

	@SerializedName("descriptionOfGoods")
	private String descriptionOfGoods;

	@SerializedName("piecesOrGrams")
	private int piecesOrGrams;

	@SerializedName("weightOfGoods")
	private double weightOfGoods;

	@SerializedName("numOfGoods")
	private int numOfGoods;

	@SerializedName("basePrice")
	private double basePrice;

	@SerializedName("supplier")
	private Supplier supplier;

	@SerializedName("manufacturer")
	private Manufacturer manufacturer;

	@SerializedName("manufacturingDate")
	private String manufacturingDate;

	@SerializedName("applicationTime")
	private double applicationTime;

	@SerializedName("deliveryDateToGrocery")
	private String deliveryDateToGrocery;

	private long goods_id = -1;
	public Goods(String nameOfGoods, String descriptionOfGoods,
				 int piecesOrGrams, double weightOfGoods, int numOfGoods,
				 double basePrice, Supplier supplier,Manufacturer manufacturer,  String manufacturingDate,
				 double applicationTime, String deliveryDateToGrocery ) {
		this.nameOfGoods = nameOfGoods;
		this.descriptionOfGoods = descriptionOfGoods;
		this.piecesOrGrams = piecesOrGrams;
		this.weightOfGoods = weightOfGoods;
		this.numOfGoods = numOfGoods;
		this.basePrice = basePrice;
		this.supplier = supplier;
		this.manufacturer = manufacturer;
		this.manufacturingDate = manufacturingDate;
		this.applicationTime = applicationTime;
		this.deliveryDateToGrocery = deliveryDateToGrocery;
	}
	public String getNameOfGoods() {
		return nameOfGoods;
	}

	public void setNameOfGoods(String nameOfGoods) {
		this.nameOfGoods = nameOfGoods;
	}

	public String getDescriptionOfGoods() {
		return descriptionOfGoods;
	}

	public void setDescriptionOfGoods(String descriptionOfGoods) {
		this.descriptionOfGoods = descriptionOfGoods;
	}
	public int getPiecesOrGrams() {
		return piecesOrGrams;
	}

	public void setNameOfGoods(int piecesOrGrams) {
		this.piecesOrGrams = piecesOrGrams;
	}

	public double getWeightOfGoods() {
		return weightOfGoods;
	}

	public void setWeightOfGoods(double weightOfGoods) {
		this.weightOfGoods = weightOfGoods;
	}

	public int getNumOfGoods() {
		return numOfGoods;
	}

	public void setNumOfGoods(int numOfGoods) {
		this.numOfGoods = numOfGoods;
	}
	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getManufacturingDate() {
		return manufacturingDate;
	}

	public void setManufacturingDate(String manufacturingDate) {
		this.manufacturingDate = manufacturingDate;
	}
	public double getApplicationTime() {
		return applicationTime;
	}

	public void setApplicationTime(double applicationTime) {
		this.applicationTime = applicationTime;
	}
	public String getDeliveryDateToGrocery() {
		return deliveryDateToGrocery;
	}

	public void setDeliveryDateToGrocery(String deliveryDateToGrocery) {
		this.deliveryDateToGrocery = deliveryDateToGrocery;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass())
			return false;
		final Goods other = (Goods) obj;

		if (!this.nameOfGoods.equals(other.nameOfGoods)) {
			return false;
		}
		if (!this.descriptionOfGoods.equals(other.descriptionOfGoods)) {
			return false;
		}
		if (this.piecesOrGrams != other.piecesOrGrams) {
			return false;
		}
		if (this.applicationTime != other.applicationTime) {
			return false;
		}
		return true;
	}
}