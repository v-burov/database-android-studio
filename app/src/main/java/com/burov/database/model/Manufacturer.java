package com.burov.database.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vitaliy on 30-Jan-16.
 */
public class Manufacturer {

    @SerializedName("nameOfManufacturer")
    String nameOfManufacturer;

    @SerializedName("descriptionOfManufacturer")
    String descriptionOfManufacturer;

    public Manufacturer(String name, String description) {
        this.nameOfManufacturer = name;
        this.descriptionOfManufacturer = description;
    }
    public String getName() {
        return nameOfManufacturer;
    }
    public void setName(String name) {
        this.nameOfManufacturer = name;
    }
    public String getDescription() {
        return descriptionOfManufacturer;
    }
    public void setDescription(String description) {
        this.descriptionOfManufacturer = description;
    }
}
