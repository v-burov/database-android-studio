package com.burov.database.model;

import com.google.gson.annotations.SerializedName;

public class Supplier {
	@SerializedName("nameOfSupplier")
	String nameOfSupplier;

	@SerializedName("descriptionOfSupplier")
	String descriptionOfSupplier;

	public Supplier(String name, String description) {
		this.nameOfSupplier = name;
		this.descriptionOfSupplier = description;
	}
	public String getName() {
		return nameOfSupplier;
	}
	public void setName(String name) {
		this.nameOfSupplier = name;
	}
	public String getDescription() {
		return descriptionOfSupplier;
	}
	public void setDescription(String description) {
		this.descriptionOfSupplier = description;
	}
}
