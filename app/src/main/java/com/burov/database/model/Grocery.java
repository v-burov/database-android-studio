package com.burov.database.model;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

public class Grocery {

	@SerializedName("typeOfGrocery")
	private int typeOfGrocery;

	@SerializedName("nameOfGrocery")
	private String nameOfGrocery;

	@SerializedName("address")
	private String address;

	@SerializedName("markup")
	private double markup;


	public Grocery(int typeOfGrocery, String name, String address, double markup) {
		this.typeOfGrocery = typeOfGrocery;
		this.nameOfGrocery = name;
		this.address = address;
		this.markup = markup;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return nameOfGrocery;
	}

	public void setName(String name) {
		this.nameOfGrocery = name;
	}

	public double getMarkup() {
		return markup;
	}

	public void setMarkup(double markup) {
		this.markup = markup;
	}

	public int getTypeOfGrocery() {
		return typeOfGrocery;
	}

	public void setTypeOfGrocery(int typeOfGrocery) {
		this.typeOfGrocery = typeOfGrocery;
	}



	public class GoodsOutOfBoundsExeption extends Exception {
		private static final long serialVersionUID = 7526472295622776147L;
		public GoodsOutOfBoundsExeption() {
			super();
		}
		public GoodsOutOfBoundsExeption(String message) {
			super(message);
		}

	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grocery other = (Grocery) obj;

		if (!this.nameOfGrocery.equals(other.nameOfGrocery)) {
			return false;
		}
		return true;
	}
	/*
	private double getPriceOfGoodsinGrocery (String nameOfGoods, double weightOfGoods) throws GoodsOutOfBoundsExeption {
		double price = 0.00d;

		for (int i = 0; i < goods.size(); i++) {
			if (goods.get(i).getNameOfGoods().equals(nameOfGoods)) {
				if (Double.doubleToLongBits(goods.get(i).getWeightOfGoods()) < Double.doubleToLongBits(weightOfGoods)) {
					throw new GoodsOutOfBoundsExeption();
				}
				else {
					price = goods.get(i).getBasePrice()*weightOfGoods + markup;
					break;
				}
			}
		}
		return price;

	}
	private double getPriceOfGoodsInGrocery (String nameOfGoods, int numberOfGoods) throws GoodsOutOfBoundsExeption {
		double price = 0.00d;
		for (int i = 0; i < goods.size(); i++) {
			if (goods.get(i).getNameOfGoods().equals(nameOfGoods)) {
				if (goods.get(i).getNumOfGoods() < numberOfGoods) {
					throw new GoodsOutOfBoundsExeption();
				}
				else {
					price = goods.get(i).getBasePrice()*numberOfGoods + markup;
					break;
				}

			}
		}
		return price;
	}
	/*
	private double getTotalPriceOfGoodsInGrocery() {
		double totalPrice = 0.00d;
		for (int i = 0; i < goods.size(); i++) {
			totalPrice += goods.get(i).getPriceOfGoods() + markup;
		}
		return totalPrice;
	}*/

	private static Date Date(int i, int j, int k) {
		// TODO Auto-generated method stub
		return null;
	}
}