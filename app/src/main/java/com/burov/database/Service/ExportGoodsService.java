package com.burov.database.Service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.widget.Toast;

import com.burov.database.R;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.model.Goods;
import com.burov.database.model.Grocery;
import com.burov.database.model.Manufacturer;
import com.burov.database.model.Supplier;
import com.burov.database.provider.DataBaseContentProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vitaliy on 22-Feb-16.
 */
public class ExportGoodsService extends IntentService {
    private static final String SAVING_PATH = "savingPath";
    private final String LOG_TAG = "myLogs";
    private static final String GROCERY_ID = "groceryId";
    private long idOfGrocery;
    Handler mHandler;
    Cursor cursor;
    ContentResolver cr;
    private String nameOfGrocery;

    public ExportGoodsService() {
        super("com.burov.database");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
        cr = getContentResolver();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String path = intent.getStringExtra(SAVING_PATH);
        idOfGrocery = intent.getLongExtra(GROCERY_ID, 0l);
        Uri singleGroceryUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, idOfGrocery);
        cursor = cr.query(singleGroceryUri, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                nameOfGrocery = cursor.getString(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GROCERY));
            } while (cursor.moveToNext());
        }
        try {
            FileWriter file = new FileWriter(path + "/" + nameOfGrocery + "." + "json");
            file.write(exportGoodsInGrocery());
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private String exportGoodsInGrocery() {
        String json = "";
        Gson gson = new Gson();
        ArrayList<Goods> listOfGoods = new ArrayList<>();
        Uri singleGroceryUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, idOfGrocery);
        cursor = cr.query(singleGroceryUri, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                    String nameOfGoods = cursor.getString(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GOODS));
                    String descriptionOfGoods = cursor.getString(cursor.
                            getColumnIndexOrThrow(DataBaseHelper.DESCRIPTION_OF_GOODS));
                    int piecesOrGrams = cursor.getInt(cursor.
                            getColumnIndexOrThrow(DataBaseHelper.PIECES_OR_GRAMS));
                    double weightOfGoods = cursor.getDouble(cursor.
                            getColumnIndexOrThrow(DataBaseHelper.WEIGHT_OF_GOODS));
                    int numOfGoods = cursor.getInt(cursor.
                            getColumnIndexOrThrow(DataBaseHelper.NUMBER_OF_GOODS));
                    double basePrice = cursor.getDouble(cursor.
                            getColumnIndexOrThrow(DataBaseHelper.BASE_PRICE));

                    String nameOfSupplier = cursor.getString(cursor.
                            getColumnIndexOrThrow(DataBaseHelper.NAME_OF_SUPPLIER));

                    String descriptionOfSupplier = cursor.getString(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.DESCRIPTION_OF_SUPPLIER));

                    String nameOfManufacturer = cursor.getString(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.NAME_OF_MANUFACTURER));

                    String descriptionOfManufacturer = cursor.getString(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.DESCRIPTION_OF_MANUFACTURER));

                    String manufacturingDate = cursor.getString(cursor.
                            getColumnIndexOrThrow(DataBaseHelper.MANUFACTURING_DATE));
                    double applicationTime = cursor.getDouble(cursor.
                            getColumnIndexOrThrow(DataBaseHelper.APPLICATION_TIME));
                    String deliveryDateToGrocery = cursor.getString(cursor.
                            getColumnIndexOrThrow(DataBaseHelper.DELIVERY_DATE_TO_GROCERY));

                    listOfGoods.add(new Goods(nameOfGoods, descriptionOfGoods, piecesOrGrams,
                            weightOfGoods, numOfGoods, basePrice, new Supplier(nameOfSupplier,
                            descriptionOfSupplier), new Manufacturer(nameOfManufacturer, descriptionOfManufacturer),
                            manufacturingDate, applicationTime, deliveryDateToGrocery));
            }while (cursor.moveToNext());
            cursor.close();
            Type type = new TypeToken<List<Goods>>() {}.getType();
            json = gson.toJson(listOfGoods, type);
        }
        else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ExportGoodsService.this, getResources().
                            getString(R.string.no_goods_to_export), Toast.LENGTH_LONG).show();
                }
            });
            stopSelf();
        }
        return json;
    }
}
