package com.burov.database.Service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.burov.database.R;
import com.burov.database.interfaces.ApiEndpointInterface;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.model.Goods;
import com.burov.database.provider.DataBaseContentProvider;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Vitaliy on 01-Mar-16.
 */
public class LoadGoodsFromServer extends IntentService {
    private final String LOG_TAG = "myLogs";
    private static final String BASE_URL = "http://share.strikersoft.com/a/androidtest/";
    ContentValues cv;
    private static final String GROCERY_ID = "groceryId";
    Handler mHandler;
    private long idOfGrocery;
    Cursor cursor;
    private long goodsGroceryId;
    ContentResolver cr;
    private String nameOfGrocery;
    public LoadGoodsFromServer() {
        super("com.burov.database");
    }
    @Override
    public void onCreate() {
        super.onCreate();
        cv = new ContentValues();
        mHandler = new Handler();
        cr = getContentResolver();
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        idOfGrocery = intent.getLongExtra(GROCERY_ID, 0l);
        cursor = cr.query(DataBaseContentProvider.GROCERY_CONTENT_URI, null, DataBaseHelper.GROCERY_ID+"=?",
                new String[]{String.valueOf(idOfGrocery)}, null);
        if (cursor != null && cursor.moveToFirst()) {

            nameOfGrocery = cursor.getString(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GROCERY)) + ".json";
        }
        else {
            Log.d(LOG_TAG, "cursor is empty");
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiEndpointInterface apiService =
                retrofit.create(ApiEndpointInterface.class);

        Call<ArrayList<Goods>> call = apiService.getListOfGoods(nameOfGrocery);

        call.enqueue(new Callback<ArrayList<Goods>>() {
            @Override
            public void onResponse(Call<ArrayList<Goods>> call, Response<ArrayList<Goods>> response) {
                if (response.isSuccess()) {
                    insertToDataBase(response.body());
                } else {

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoadGoodsFromServer.this, getResources().
                                    getString(R.string.no_goods_in_server), Toast.LENGTH_LONG).show();
                        }
                    });
                    stopSelf();

                }
            }
            @Override
            public void onFailure(Call<ArrayList<Goods>> call, Throwable t) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoadGoodsFromServer.this, getResources().
                                getString(R.string.no_goods_in_server), Toast.LENGTH_LONG).show();
                    }
                });
                stopSelf();
            }
        });
    }

    public void insertToDataBase (ArrayList<Goods> listOfGoods) {
        if (listOfGoods.isEmpty()){
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(LoadGoodsFromServer.this, getResources().
                            getString(R.string.no_goods_in_server), Toast.LENGTH_LONG).show();
                }
            });
            stopSelf();
        }
        String nameOfGoods;
        String descriptionOfGoods;
        int piecesOrGrams;
        double applicationTime;

        ArrayList<Goods> goodsNameInDatabase = new ArrayList<>();
        long goodsId = -1;

        String [] projection = new String[] {String.valueOf(DataBaseHelper.GOODS_ID)};
        String [] selectionArgs;
        String selection = DataBaseHelper.GOODS_ID + "=?";
        ContentResolver cr = getContentResolver();
        //get the array of the name of the groceries that are in a database
        Uri singleGroceryUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, idOfGrocery);
        cursor = cr.query(singleGroceryUri, null, null, null, null);//REVIEW:need close cursor for every query in finally block

        if (cursor != null && cursor.moveToFirst()) {
            do {
                nameOfGoods= cursor.getString(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GOODS));
                descriptionOfGoods= cursor.getString(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.DESCRIPTION_OF_GOODS));
                piecesOrGrams= cursor.getInt(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.PIECES_OR_GRAMS));
                applicationTime= cursor.getDouble(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.APPLICATION_TIME));

                goodsNameInDatabase.add(new Goods(nameOfGoods, descriptionOfGoods, piecesOrGrams, 0, 0, 0,
                        null, null,null,applicationTime, null));
            } while (cursor.moveToNext());
            cursor.close();
        }

        //update the exist goods, insert new
        for (int i = 0; i < listOfGoods.size(); i++) {
            long gmGoodsId = 0l;
            String manufacturingDate = "";
            long goodsManufacturingId = 0l;
            long gsGoodsManufacturingId = 0l;
            double basePrice = 0;
            long goodsSupplierId = 0l;
            long ggGroceryId = 0l;
            long ggGoodsSupplierId = 0l;
            double weightOfGoods = 0;
            int numOfGoods = 0;
            String deliveryDateToGrocery = "";
            long manufacturerId = 0l;
            String nameOfSupplier = listOfGoods.get(i).getSupplier().getName();
            String descriptionOfSupplier = listOfGoods.get(i).getSupplier().getDescription();
            long supplierId = 0l;
            String nameOfManufacturer = listOfGoods.get(i).getManufacturer().getName();
            String descriptionOfManufacturer = listOfGoods.get(i).getManufacturer().getDescription();
            cursor = cr.query(DataBaseContentProvider.SUPPLIER_CONTENT_URI, null,
                    DataBaseHelper.NAME_OF_SUPPLIER + "=?", new String[]{nameOfSupplier}, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    supplierId = cursor.getLong(cursor.
                            getColumnIndexOrThrow(DataBaseHelper.SUPPLIER_ID));
                } while (cursor.moveToNext());
                cursor.close();
            } else {
                cv.clear();
                cv.put(DataBaseHelper.NAME_OF_SUPPLIER, nameOfSupplier);
                cv.put(DataBaseHelper.DESCRIPTION_OF_SUPPLIER, descriptionOfSupplier);
                cr.insert(DataBaseContentProvider.SUPPLIER_CONTENT_URI, cv);
                cursor = cr.query(DataBaseContentProvider.SUPPLIER_CONTENT_URI, null,
                        DataBaseHelper.NAME_OF_SUPPLIER + "=?", new String[]{nameOfSupplier}, null);
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        supplierId = cursor.getLong(cursor.
                                getColumnIndexOrThrow(DataBaseHelper.SUPPLIER_ID));
                    } while (cursor.moveToNext());
                    cursor.close();
                }
            }
            cursor = cr.query(DataBaseContentProvider.MANUFACTURER_CONTENT_URI, null,
                    DataBaseHelper.NAME_OF_MANUFACTURER + "=?", new String[]{nameOfManufacturer}, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    manufacturerId = cursor.getLong(cursor.
                            getColumnIndexOrThrow(DataBaseHelper.MANUFACTURER_ID));
                } while (cursor.moveToNext());
                cursor.close();
            } else {
                cv.clear();
                cv.put(DataBaseHelper.NAME_OF_MANUFACTURER, nameOfManufacturer);
                cv.put(DataBaseHelper.DESCRIPTION_OF_MANUFACTURER, descriptionOfManufacturer);
                cr.insert(DataBaseContentProvider.MANUFACTURER_CONTENT_URI, cv);
                cursor = cr.query(DataBaseContentProvider.MANUFACTURER_CONTENT_URI, null,
                        DataBaseHelper.NAME_OF_MANUFACTURER + "=?", new String[]{nameOfManufacturer}, null);
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        manufacturerId = cursor.getLong(cursor.
                                getColumnIndexOrThrow(DataBaseHelper.MANUFACTURER_ID));
                    } while (cursor.moveToNext());
                    cursor.close();
                }
            }
            cv.clear();
            cv.put(DataBaseHelper.GOODS_ID, goodsId);
            cv.put(DataBaseHelper.NAME_OF_GOODS, listOfGoods.get(i).getNameOfGoods());
            cv.put(DataBaseHelper.DESCRIPTION_OF_GOODS, listOfGoods.get(i).getDescriptionOfGoods());
            cv.put(DataBaseHelper.PIECES_OR_GRAMS, listOfGoods.get(i).getPiecesOrGrams());
            cv.put(DataBaseHelper.WEIGHT_OF_GOODS, listOfGoods.get(i).getWeightOfGoods());
            cv.put(DataBaseHelper.NUMBER_OF_GOODS, listOfGoods.get(i).getNumOfGoods());
            cv.put(DataBaseHelper.BASE_PRICE, listOfGoods.get(i).getBasePrice());
            cv.put(DataBaseHelper.SUPPLIER_ID, supplierId);
            cv.put(DataBaseHelper.MANUFACTURER_ID, manufacturerId);
            cv.put(DataBaseHelper.MANUFACTURING_DATE, listOfGoods.get(i).getManufacturingDate());
            cv.put(DataBaseHelper.APPLICATION_TIME, listOfGoods.get(i).getApplicationTime());
            cv.put(DataBaseHelper.DELIVERY_DATE_TO_GROCERY, listOfGoods.get(i).getDeliveryDateToGrocery());

            selectionArgs = new String[]{listOfGoods.get(i).getNameOfGoods()};
            cursor = cr.query(DataBaseContentProvider.GOODS_CONTENT_URI, null,
                    DataBaseHelper.NAME_OF_GOODS + "=?", selectionArgs, null);

            if (cursor != null && cursor.moveToFirst()) {
                goodsId = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_ID));
            }
            manufacturingDate = listOfGoods.get(i).getManufacturingDate();
            basePrice = listOfGoods.get(i).getBasePrice();
            weightOfGoods = listOfGoods.get(i).getWeightOfGoods();
            numOfGoods = listOfGoods.get(i).getNumOfGoods();
            deliveryDateToGrocery = listOfGoods.get(i).getDeliveryDateToGrocery();

            if (goodsNameInDatabase.contains(listOfGoods.get(i))) {
                Uri singleGoods = ContentUris.withAppendedId(DataBaseContentProvider.GOODS_CONTENT_URI, goodsId);
                cursor = cr.query(singleGoods,null,null,null,null);
                if (cursor != null && cursor.moveToFirst()) {
                    goodsGroceryId = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_GROCERY_ID));
                }

                Uri singleUri = ContentUris.withAppendedId(DataBaseContentProvider.GOODS_GROCERY_CONTENT_URI, goodsGroceryId);
                cursor = cr.query(singleUri, null, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                        Log.d(LOG_TAG, "Update");
                        gmGoodsId = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GM_GOODS_ID));
                        goodsManufacturingId = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_MANUFACTURING_ID));
                        gsGoodsManufacturingId = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GS_GOODS_MANUFACTURING_ID));
                        goodsSupplierId = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_SUPPLIER_ID));
                        ggGroceryId = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GG_GROCERY_ID));
                        ggGoodsSupplierId = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseHelper.GG_GOODS_SUPPLIER_ID));
                        cv.put(DataBaseHelper.GM_MANUFACTURER_ID, manufacturerId);
                        cv.put(DataBaseHelper.GM_GOODS_ID, gmGoodsId);
                        cv.put(DataBaseHelper.MANUFACTURING_DATE, manufacturingDate);
                        cv.put(DataBaseHelper.GOODS_MANUFACTURING_ID, goodsManufacturingId);

                        cv.put(DataBaseHelper.GS_SUPPLIER_ID, supplierId);
                        cv.put(DataBaseHelper.GS_GOODS_MANUFACTURING_ID, gsGoodsManufacturingId);
                        cv.put(DataBaseHelper.BASE_PRICE, basePrice);
                        cv.put(DataBaseHelper.GOODS_SUPPLIER_ID, goodsSupplierId);

                        cv.put(DataBaseHelper.GG_GROCERY_ID, ggGroceryId);
                        cv.put(DataBaseHelper.GG_GOODS_SUPPLIER_ID, ggGoodsSupplierId);
                        cv.put(DataBaseHelper.WEIGHT_OF_GOODS, weightOfGoods);
                        cv.put(DataBaseHelper.NUMBER_OF_GOODS, numOfGoods);
                        cv.put(DataBaseHelper.DELIVERY_DATE_TO_GROCERY, deliveryDateToGrocery);
                        Uri updateUri = ContentUris.withAppendedId(DataBaseContentProvider.GOODS_GROCERY_CONTENT_URI, goodsGroceryId);
                        cr.update(updateUri, cv, null, null);
                        Log.d(LOG_TAG, goodsGroceryId + " ");
                        goodsNameInDatabase.remove(listOfGoods.get(i));
                }
            }
            else {
                Log.d(LOG_TAG, "insert new goods");
                try {
                    Uri insertUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, idOfGrocery);
                    cr.insert(insertUri, cv);
                }catch (SQLiteConstraintException e) {
                    cv.clear();
                    Log.d(LOG_TAG, "Insert exist goods");
                    cv.put(DataBaseHelper.GOODS_ID, goodsId);
                    cv.put(DataBaseHelper.WEIGHT_OF_GOODS, weightOfGoods);
                    cv.put(DataBaseHelper.NUMBER_OF_GOODS, numOfGoods);
                    cv.put(DataBaseHelper.BASE_PRICE, basePrice);
                    cv.put(DataBaseHelper.SUPPLIER_ID, supplierId);
                    cv.put(DataBaseHelper.MANUFACTURER_ID, manufacturerId);
                    cv.put(DataBaseHelper.MANUFACTURING_DATE, manufacturingDate);
                    cv.put(DataBaseHelper.DELIVERY_DATE_TO_GROCERY, deliveryDateToGrocery);
                    Uri InsertUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, idOfGrocery);
                    cr.insert(InsertUri, cv);
                    goodsNameInDatabase.remove(listOfGoods.get(i));
                }
            }
        }
        //deleting grocery name from array of the name of the groceries that are updated
        for (int i = 0; i < goodsNameInDatabase.size(); i++) {
            selectionArgs = new String[]{goodsNameInDatabase.get(i).getNameOfGoods()};

            cursor = cr.query(DataBaseContentProvider.GOODS_CONTENT_URI, null,
                    DataBaseHelper.NAME_OF_GOODS + "=?", selectionArgs, null);

            if (cursor != null && cursor.moveToFirst()) {
                goodsId = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_ID));
            }
            Uri singleGoods = ContentUris.withAppendedId(DataBaseContentProvider.GOODS_CONTENT_URI, goodsId);
            cursor = cr.query(singleGoods, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    goodsGroceryId = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GOODS_GROCERY_ID));
                }while (cursor.moveToNext());
            }
            Log.d(LOG_TAG, goodsGroceryId + " id");
            singleGroceryUri = ContentUris.withAppendedId(DataBaseContentProvider.GOODS_GROCERY_CONTENT_URI, goodsGroceryId);
                cr.delete(singleGroceryUri, null, null);
        }
    }
}
