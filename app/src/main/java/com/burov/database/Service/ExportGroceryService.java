package com.burov.database.Service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.widget.Toast;
import com.burov.database.R;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.model.Grocery;
import com.burov.database.provider.DataBaseContentProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vitaliy on 17-Feb-16.
 */
public class ExportGroceryService extends IntentService {
    private static final String SAVING_PATH = "savingPath";
    private final String LOG_TAG = "myLogs";
    private JSONArray jsonArrayGroceries;
    Handler mHandler;


    public ExportGroceryService() {
        super("com.burov.database");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String path = intent.getStringExtra(SAVING_PATH);
        try {
            FileWriter file = new FileWriter(path+"/groceries"+"."+"json");
            file.write(loadGroceries());
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private String loadGroceries () {
        jsonArrayGroceries = new JSONArray();
        ArrayList<Grocery> listOfGroceries = new ArrayList<>();
        Cursor cursor;
        Gson gson = new Gson();
        ContentResolver cr = getContentResolver();
        cursor = cr.query(DataBaseContentProvider.GROCERY_CONTENT_URI, null, null, null, null);
        String json = "";
        if (cursor != null && cursor.moveToFirst()) {
            do {
                int typeOfGrocery = cursor.getInt(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.TYPE_OF_GROCERY));
                String nameOfGrocery = cursor.getString(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GROCERY));
                String address = cursor.
                        getString(cursor.getColumnIndexOrThrow(DataBaseHelper.ADDRESS));
                double markup = cursor.
                        getDouble(cursor.getColumnIndexOrThrow(DataBaseHelper.MARKUP));

                listOfGroceries.add(new Grocery(typeOfGrocery, nameOfGrocery, address, markup));

                //jsonArrayGroceries.put(gson.toJson(new Grocery(typeOfGrocery, nameOfGrocery, address, markup)));

            }while (cursor.moveToNext());
            Type type = new TypeToken<List<Grocery>>() {}.getType();
            json = gson.toJson(listOfGroceries, type);
            cursor.close();
        }
        else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ExportGroceryService.this,getResources().
                            getString(R.string.no_groceries_to_export), Toast.LENGTH_LONG).show();
                }
            });
            stopSelf();
        }
        return json;
    }
}