package com.burov.database.Service;
import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.burov.database.R;
import com.burov.database.interfaces.ApiEndpointInterface;
import com.burov.database.model.DataBaseHelper;
import com.burov.database.model.Grocery;
import com.burov.database.provider.DataBaseContentProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Vitaliy on 25-Feb-16.
 */
public class LoadGroceriesFromServer extends IntentService {
    private final String LOG_TAG = "myLogs";
    private static final String BASE_URL = "http://share.strikersoft.com/a/androidtest/";
    ContentValues cv;
    Handler mHandler;
    public LoadGroceriesFromServer() {
        super("com.burov.database");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        cv = new ContentValues();
        mHandler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiEndpointInterface apiService =
                retrofit.create(ApiEndpointInterface.class);

        Call<ArrayList<Grocery>> call = apiService.getListOfGrocery();
        call.enqueue(new Callback<ArrayList<Grocery>>() {
            @Override
            public void onResponse(Call<ArrayList<Grocery>> call, Response<ArrayList<Grocery>> response) {
                if (response.isSuccess()) {
                    insertToDataBase(response.body());
                } else {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoadGroceriesFromServer.this,getResources().
                                    getString(R.string.no_groceries_in_server), Toast.LENGTH_LONG).show();
                        }
                    });
                    stopSelf();
                }
            }
            @Override
            public void onFailure(Call<ArrayList<Grocery>> call, Throwable t) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoadGroceriesFromServer.this,getResources().
                                getString(R.string.no_groceries_in_server), Toast.LENGTH_LONG).show();
                    }
                });
                stopSelf();
            }
        });
    }

    public void insertToDataBase (ArrayList<Grocery> groceries ) {
        Cursor cursor;
        String nameOfGrocery = null;
        ArrayList<String> groceriesInDatabase = new ArrayList<>();
        long idOfGrocery = 0l;
        String [] projection = new String[] {String.valueOf(DataBaseHelper.GROCERY_ID)};
        String [] selectionArgs;
        String selection;
        ContentResolver cr = getContentResolver();
        //get the array of the name of the groceries that are in a database
        cursor = cr.query(DataBaseContentProvider.GROCERY_CONTENT_URI, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                nameOfGrocery = cursor.getString(cursor.
                        getColumnIndexOrThrow(DataBaseHelper.NAME_OF_GROCERY));
                groceriesInDatabase.add(nameOfGrocery);
            } while (cursor.moveToNext());
            cursor.close();
        }
        //update the exist groceries, insert new
        for (int i = 0; i < groceries.size(); i++) {
            selection = DataBaseHelper.NAME_OF_GROCERY + "=?";
            selectionArgs = new String[] {groceries.get(i).getName()};
            cursor = cr.query(DataBaseContentProvider.GROCERY_CONTENT_URI, projection,
                    selection, selectionArgs, null);

            if (cursor!=null && cursor.moveToFirst()) {
                idOfGrocery = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GROCERY_ID));

            }
            cv.put(DataBaseHelper.TYPE_OF_GROCERY, groceries.get(i).getTypeOfGrocery());
            cv.put(DataBaseHelper.NAME_OF_GROCERY, groceries.get(i).getName());
            cv.put(DataBaseHelper.ADDRESS, groceries.get(i).getAddress());
            cv.put(DataBaseHelper.MARKUP, groceries.get(i).getMarkup());

            if (groceriesInDatabase.contains(groceries.get(i).getName())) {
                selection= DataBaseHelper.GROCERY_ID + "=?";
                Uri singleUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, idOfGrocery);
                cr.update(singleUri, cv, selection, new String[]{String.valueOf(idOfGrocery)});
                groceriesInDatabase.remove(groceries.get(i).getName());
            }
            else {
                cr.insert(DataBaseContentProvider.GROCERY_CONTENT_URI, cv);
            }
        }
        //deleting grocery name from array of the name of the groceries that are updated
        for (int i = 0; i < groceriesInDatabase.size(); i++) {
            selection = DataBaseHelper.NAME_OF_GROCERY + "=?";
            selectionArgs = new String[]{groceriesInDatabase.get(i)};
            cursor = cr.query(DataBaseContentProvider.GROCERY_CONTENT_URI, projection,
                    selection, selectionArgs, null);
            if (cursor!=null && cursor.moveToFirst()) {
                idOfGrocery = cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.GROCERY_ID));
            }
            Uri singleGroceryUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, idOfGrocery);
            selection = DataBaseHelper.GROCERY_ID + " = ?";
            cr.delete(singleGroceryUri, selection, new String[]{String.valueOf(idOfGrocery)});
        }
    }

}