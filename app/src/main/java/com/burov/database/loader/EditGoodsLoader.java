package com.burov.database.loader;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.burov.database.provider.DataBaseContentProvider;

/**
 * Created by Vitaliy on 15-Nov-15.
 */
public class EditGoodsLoader extends CursorLoader {
    Cursor cursor = null;
    private long idOfGoods = 0L;
    private final Loader.ForceLoadContentObserver observer;
    public EditGoodsLoader(Context context,long idOfGoods) {
        super(context);
        this.idOfGoods = idOfGoods;
        this.observer = new Loader.ForceLoadContentObserver();
    }
    public long getIdOfGoods() {
        return idOfGoods;
    }
    @Override
    protected void onStartLoading()  {
        if (this.cursor != null) {
            deliverResult(this.cursor);
        }
        if (takeContentChanged() || this.cursor == null) {
            forceLoad();
        }
    }

    @Override
    public Cursor loadInBackground() {
        ContentResolver cr = getContext().getContentResolver();

        Uri singleUri = ContentUris.withAppendedId(DataBaseContentProvider.GOODS_GROCERY_CONTENT_URI, getIdOfGoods());

        cursor = cr.query(singleUri, null, null, null, null);
        if (cursor != null) {
            cursor.getCount();
            cursor.registerContentObserver(this.observer);
            cursor.setNotificationUri(getContext().getContentResolver(),
                    DataBaseContentProvider.GROCERY_CONTENT_URI);
        }
        return cursor;

    }
}
