package com.burov.database.loader;

import android.content.ContentResolver;
import android.content.Context;

import android.database.Cursor;

import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.burov.database.model.DataBaseHelper;
import com.burov.database.provider.DataBaseContentProvider;

/**
 * Created by Vitaliy on 13-Dec-15.
 */
public class LoaderOfAllGoods extends CursorLoader {
    Cursor cursor = null;
    private final String LOG_TAG = "myLogs";
    private final Loader.ForceLoadContentObserver goodsObserver;
    public LoaderOfAllGoods(Context context) {
        super(context);
        this.goodsObserver = new Loader.ForceLoadContentObserver();
    }

    @Override
    protected void onStartLoading() {
        if (this.cursor != null) {
            deliverResult(this.cursor);
        }
        if (takeContentChanged() || this.cursor == null) {
            forceLoad();
        }
    }

    @Override
    public Cursor loadInBackground() {
        ContentResolver cr = getContext().getContentResolver();
        String[] projection = new String[]{DataBaseHelper.GOODS_ID+" AS _id", DataBaseHelper.NAME_OF_GOODS,
        DataBaseHelper.DESCRIPTION_OF_GOODS, DataBaseHelper.PIECES_OR_GRAMS, DataBaseHelper.APPLICATION_TIME};
        cursor = cr.query(DataBaseContentProvider.GOODS_CONTENT_URI, projection, null, null, null);
        if (cursor != null) {
            cursor.getCount();
            cursor.registerContentObserver(this.goodsObserver);
            cursor.setNotificationUri(getContext().getContentResolver(),
                    DataBaseContentProvider.GOODS_CONTENT_URI);
        }
        return cursor;
    }
}
