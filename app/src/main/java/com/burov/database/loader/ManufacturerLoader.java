package com.burov.database.loader;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.burov.database.provider.DataBaseContentProvider;

/**
 * Created by Vitaliy on 05-Dec-15.
 */
public class ManufacturerLoader extends CursorLoader {
    Cursor cursor = null;
    private String selection;
    private String[] selectionArgs;
    private String[] projection;
    private final Loader.ForceLoadContentObserver observer;
    public ManufacturerLoader(Context context, String projection[], String selection, String selectionArgs[]) {
        super(context);
        this.selection = selection;
        this.selectionArgs = selectionArgs;
        this.projection = projection;
        this.observer = new Loader.ForceLoadContentObserver();
    }

    //get SelectionArgs
    @Override
    public String[] getSelectionArgs() {
        return selectionArgs;
    }

    //get Projection
    @Override
    public String[] getProjection() { return  projection; }

    @Override
    protected void onStartLoading() {
        if (this.cursor != null) {
            deliverResult(this.cursor);
        }
        if (takeContentChanged() || this.cursor == null) {
            forceLoad();
        }
    }

    @Override
    public Cursor loadInBackground() {
        ContentResolver cr = getContext().getContentResolver();

        cursor = cr.query(DataBaseContentProvider.MANUFACTURER_CONTENT_URI, projection, selection,
                selectionArgs, null);
        if (cursor != null) {
            cursor.getCount();
            cursor.registerContentObserver(this.observer);
            cursor.setNotificationUri(getContext().getContentResolver(),
                    DataBaseContentProvider.MANUFACTURER_CONTENT_URI);
        }
        return cursor;

    }
}
