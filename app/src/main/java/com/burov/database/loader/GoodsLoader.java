package com.burov.database.loader;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import com.burov.database.provider.DataBaseContentProvider;


/**
 * Created by home on 30.08.2015.
 */
public class GoodsLoader extends CursorLoader {
    Cursor cursor = null;
    private long idOfItem;
    private final String LOG_TAG = "myLogs";
    private final ForceLoadContentObserver goodsObserver;
    public GoodsLoader(Context context, long idOfItem) {
        super(context);
        this.idOfItem = idOfItem;
        this.goodsObserver = new ForceLoadContentObserver();
    }

    public long getIdOfItem() {
        return idOfItem;
    }

    @Override
    protected void onStartLoading() {
        if (this.cursor != null) {
            deliverResult(this.cursor);
        }
        if (takeContentChanged() || this.cursor == null) {
            forceLoad();
        }
    }

    @Override
    public Cursor loadInBackground() {
        long idOfItem = getIdOfItem();
        ContentResolver cr = getContext().getContentResolver();
        Uri singleGroceryUri = ContentUris.withAppendedId(DataBaseContentProvider.GROCERY_CONTENT_URI, idOfItem);
        cursor = cr.query(singleGroceryUri, null, null, null, null);

        if (cursor != null) {
            cursor.getCount();
            cursor.registerContentObserver(this.goodsObserver);
            cursor.setNotificationUri(getContext().getContentResolver(),
                    singleGroceryUri);
        }
        return cursor;

    }

}
