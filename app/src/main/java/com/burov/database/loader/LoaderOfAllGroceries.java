package com.burov.database.loader;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.burov.database.provider.DataBaseContentProvider;

/**
 * Created by Vitaliy on 06-Feb-16.
 */
public class LoaderOfAllGroceries extends CursorLoader {
    Cursor cursor = null;
    private final Loader.ForceLoadContentObserver observer;
    public LoaderOfAllGroceries(Context context) {
        super(context);
        this.observer = new Loader.ForceLoadContentObserver();
    }

    @Override
    protected void onStartLoading() {
        if (this.cursor != null) {
            deliverResult(this.cursor);
        }
        if (takeContentChanged() || this.cursor == null) {
            forceLoad();
        }
    }

    @Override
    public Cursor loadInBackground() {
        ContentResolver cr = getContext().getContentResolver();
        cursor = cr.query(DataBaseContentProvider.GROCERY_CONTENT_URI, null, null, null, null);
        if (cursor != null) {
            cursor.getCount();
            cursor.registerContentObserver(this.observer);
            cursor.setNotificationUri(getContext().getContentResolver(),
                    DataBaseContentProvider.GROCERY_CONTENT_URI);
        }
        return cursor;
    }
}
