package com.burov.database.loader;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.burov.database.model.DataBaseHelper;
import com.burov.database.provider.DataBaseContentProvider;

/**
 * Created by Vitaliy on 01-Jan-16.
 */
public class InsertExistGoodsLoader extends CursorLoader {
    Cursor cursor = null;
    private String [] projection;
    private String selection;
    private String [] selectionArgs;

    final String LOG_TAG = "myLogs";
    final Loader.ForceLoadContentObserver goodsObserver;
    public InsertExistGoodsLoader(Context context, String [] projection, String selection, String[] selectionArgs) {
        super(context);
        this.projection = projection;
        this.selection = selection;
        this.selectionArgs = selectionArgs;
        this.goodsObserver = new Loader.ForceLoadContentObserver();
    }

    @Override
    protected void onStartLoading() {
        if (this.cursor != null) {
            deliverResult(this.cursor);
        }
        if (takeContentChanged() || this.cursor == null) {
            forceLoad();
        }
    }

    @Override
    public Cursor loadInBackground() {
        ContentResolver cr = getContext().getContentResolver();
        cursor = cr.query(DataBaseContentProvider.GOODS_CONTENT_URI, projection, selection, selectionArgs, null);
        if (cursor != null) {
            cursor.getCount();
            cursor.registerContentObserver(this.goodsObserver);
            cursor.setNotificationUri(getContext().getContentResolver(),
                    DataBaseContentProvider.GOODS_CONTENT_URI);
        }
        return cursor;
    }
}
