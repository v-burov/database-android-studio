package com.burov.database.loader;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import com.burov.database.provider.DataBaseContentProvider;


/**
 * Created by home on 28.08.2015.
 */
public class GroceryLoader extends CursorLoader {
    Cursor cursor = null;
    private String selection;
    private String[] selectionArgs;
    private String[] projection;
    private final ForceLoadContentObserver observer;
    public GroceryLoader(Context context,String projection[], String selection, String [] selectionArgs) {
        super(context);
        this.selection = selection;
        this.selectionArgs = selectionArgs;
        this.projection = projection;
        this.observer = new ForceLoadContentObserver();
    }
    //get SelectionArgs
    @Override
    public String[] getSelectionArgs() {
        return selectionArgs;
    }

    @Override
    public String[] getProjection() { return  projection; }

    @Override
    protected void onStartLoading() {
        if (this.cursor != null) {
            deliverResult(this.cursor);
        }
        if (takeContentChanged() || this.cursor == null) {
            forceLoad();
        }
    }

    @Override
    public Cursor loadInBackground() {
        ContentResolver cr = getContext().getContentResolver();
        int tabPage = Integer.valueOf(getSelectionArgs()[0]);


        switch(tabPage) {
            case 0:
                cursor = cr.query(DataBaseContentProvider.GROCERY_CONTENT_URI, projection, selection,
                        selectionArgs, null);
                if (cursor != null) {
                    cursor.getCount();
                    cursor.registerContentObserver(this.observer);
                    cursor.setNotificationUri(getContext().getContentResolver(),
                            DataBaseContentProvider.GROCERY_CONTENT_URI);
                }
                return cursor;

            case 1:
                cursor = cr.query(DataBaseContentProvider.GROCERY_CONTENT_URI, projection, selection,
                        selectionArgs, null);
                if (cursor != null) {
                    cursor.getCount();
                    cursor.registerContentObserver(this.observer);
                    cursor.setNotificationUri(getContext().getContentResolver(),
                            DataBaseContentProvider.GROCERY_CONTENT_URI);
                }
                return cursor;

            case 2:
                cursor = cr.query(DataBaseContentProvider.GROCERY_CONTENT_URI, projection, selection,
                        selectionArgs, null);
                if (cursor != null) {
                    cursor.getCount();
                    cursor.registerContentObserver(this.observer);
                    cursor.setNotificationUri(getContext().getContentResolver(),
                            DataBaseContentProvider.GROCERY_CONTENT_URI);
                }
                return cursor;
            default:
        }
        return null;
    }
}
