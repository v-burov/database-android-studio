package com.burov.database.interfaces;

import android.support.v7.util.SortedList;

import com.burov.database.model.Goods;
import com.burov.database.model.Grocery;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Vitaliy on 24-Feb-16.
 */
public interface ApiEndpointInterface {
    /**
     * Created by Vitaliy on 24-Feb-16.
     */
        @GET("/a/androidtest/groceries.json")
        Call<ArrayList<Grocery>> getListOfGrocery();

        @GET("/a/androidtest/{nameOfGrocery}")
        Call<ArrayList<Goods>> getListOfGoods(@Path("nameOfGrocery") String nameOfGrocery);
}
